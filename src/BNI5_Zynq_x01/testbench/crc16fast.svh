`ifndef __CRC16FAST
`define __CRC16FAST


  package pack_crc16;
    
    class crc16;
        
      local bit [7:0] ptr[];
  
      local const bit[15:0] crctab[256] = {     
        16'h0000, 16'hc0c1, 16'hc181, 16'h0140, 16'hc301, 16'h03c0, 16'h0280, 16'hc241, 16'hc601, 16'h06c0, 16'h0780, 16'hc741, 16'h0500, 16'hc5c1, 16'hc481, 16'h0440,
        16'hcc01, 16'h0cc0, 16'h0d80, 16'hcd41, 16'h0f00, 16'hcfc1, 16'hce81, 16'h0e40, 16'h0a00, 16'hcac1, 16'hcb81, 16'h0b40, 16'hc901, 16'h09c0, 16'h0880, 16'hc841,
        16'hd801, 16'h18c0, 16'h1980, 16'hd941, 16'h1b00, 16'hdbc1, 16'hda81, 16'h1a40, 16'h1e00, 16'hdec1, 16'hdf81, 16'h1f40, 16'hdd01, 16'h1dc0, 16'h1c80, 16'hdc41,
        16'h1400, 16'hd4c1, 16'hd581, 16'h1540, 16'hd701, 16'h17c0, 16'h1680, 16'hd641, 16'hd201, 16'h12c0, 16'h1380, 16'hd341, 16'h1100, 16'hd1c1, 16'hd081, 16'h1040,
        16'hf001, 16'h30c0, 16'h3180, 16'hf141, 16'h3300, 16'hf3c1, 16'hf281, 16'h3240, 16'h3600, 16'hf6c1, 16'hf781, 16'h3740, 16'hf501, 16'h35c0, 16'h3480, 16'hf441,
        16'h3c00, 16'hfcc1, 16'hfd81, 16'h3d40, 16'hff01, 16'h3fc0, 16'h3e80, 16'hfe41, 16'hfa01, 16'h3ac0, 16'h3b80, 16'hfb41, 16'h3900, 16'hf9c1, 16'hf881, 16'h3840,
        16'h2800, 16'he8c1, 16'he981, 16'h2940, 16'heb01, 16'h2bc0, 16'h2a80, 16'hea41, 16'hee01, 16'h2ec0, 16'h2f80, 16'hef41, 16'h2d00, 16'hedc1, 16'hec81, 16'h2c40,
        16'he401, 16'h24c0, 16'h2580, 16'he541, 16'h2700, 16'he7c1, 16'he681, 16'h2640, 16'h2200, 16'he2c1, 16'he381, 16'h2340, 16'he101, 16'h21c0, 16'h2080, 16'he041,
        16'ha001, 16'h60c0, 16'h6180, 16'ha141, 16'h6300, 16'ha3c1, 16'ha281, 16'h6240, 16'h6600, 16'ha6c1, 16'ha781, 16'h6740, 16'ha501, 16'h65c0, 16'h6480, 16'ha441,
        16'h6c00, 16'hacc1, 16'had81, 16'h6d40, 16'haf01, 16'h6fc0, 16'h6e80, 16'hae41, 16'haa01, 16'h6ac0, 16'h6b80, 16'hab41, 16'h6900, 16'ha9c1, 16'ha881, 16'h6840,
        16'h7800, 16'hb8c1, 16'hb981, 16'h7940, 16'hbb01, 16'h7bc0, 16'h7a80, 16'hba41, 16'hbe01, 16'h7ec0, 16'h7f80, 16'hbf41, 16'h7d00, 16'hbdc1, 16'hbc81, 16'h7c40,
        16'hb401, 16'h74c0, 16'h7580, 16'hb541, 16'h7700, 16'hb7c1, 16'hb681, 16'h7640, 16'h7200, 16'hb2c1, 16'hb381, 16'h7340, 16'hb101, 16'h71c0, 16'h7080, 16'hb041,
        16'h5000, 16'h90c1, 16'h9181, 16'h5140, 16'h9301, 16'h53c0, 16'h5280, 16'h9241, 16'h9601, 16'h56c0, 16'h5780, 16'h9741, 16'h5500, 16'h95c1, 16'h9481, 16'h5440,
        16'h9c01, 16'h5cc0, 16'h5d80, 16'h9d41, 16'h5f00, 16'h9fc1, 16'h9e81, 16'h5e40, 16'h5a00, 16'h9ac1, 16'h9b81, 16'h5b40, 16'h9901, 16'h59c0, 16'h5880, 16'h9841,
        16'h8801, 16'h48c0, 16'h4980, 16'h8941, 16'h4b00, 16'h8bc1, 16'h8a81, 16'h4a40, 16'h4e00, 16'h8ec1, 16'h8f81, 16'h4f40, 16'h8d01, 16'h4dc0, 16'h4c80, 16'h8c41,
        16'h4400, 16'h84c1, 16'h8581, 16'h4540, 16'h8701, 16'h47c0, 16'h4680, 16'h8641, 16'h8201, 16'h42c0, 16'h4380, 16'h8341, 16'h4100, 16'h81c1, 16'h8081, 16'h4040
      };
  
      local function [15:0] reflect( input bit [15:0] crc, input bit[15:0] bitnum );
        bit[15:0] crc_out;
        bit[15:0] j;
        bit[15:0] i;
      
        crc_out = 0;
        j       = 1;  
        for( i = (1 << (bitnum-1)); i; i >>= 1 ) begin
          if( crc & i )
            crc_out |= j;
          j <<= 1;
        end
        return crc_out;
      endfunction   
    
      function [15:0] get_crc;
        bit[15:0] crc;
        int i;
        int len;
        
        len = ptr.size();
        i   = 0;            
        crc = 16'hFFFF;
        crc = reflect( crc, 16 );
        while( len-- ) begin
          crc = (crc>>8)^crctab[ (crc & 16'hFF)^ptr[i] ];
          i++;
        end
        crc = reflect( crc, 16 );
        crc ^= 16'hFFFF;
        return crc;
      endfunction
      
      task clear;
        ptr.delete;
      endtask
      
      task append_word( input bit [15:0] data );
        int size;
        size = ptr.size();
        ptr  = new[size+2](ptr);
        ptr[size++] = data[ 7:0];
        ptr[size++] = data[15:8];
      endtask

      task append_byte( input bit [7:0] data );
      	int size;
      	size = ptr.size();
      	ptr  = new[size+1](ptr);
      	ptr[size++] = data;
      endtask
      
    endclass

  endpackage

`endif