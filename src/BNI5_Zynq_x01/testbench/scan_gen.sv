`timescale 1ns/1ns
`include "crc16fast.svh"

import pack_crc16::*;

module scan_gen #(
  parameter START_DELAY    = 5_300_000,
  parameter INTERVAL       = 1_600_000,
  parameter FLOAT_INTERVAL =   100_000,
  parameter IS_RANDOM_ER   = "FALSE"
)(
  output bit o_tx = '1
);

  localparam
    HALF_INTERVAL = FLOAT_INTERVAL / 2;

  crc16 crc = new();

  bit [15:0] crc_calc;

  initial begin
    int  scan_num;
    int signed   val;
    int unsigned val_us;
    time t_begin;
    time t_end;    
    
    scan_num = 0;
    #(START_DELAY);
    forever begin
      t_begin = $time;
      send_scan( scan_num++, scan_num, scan_num, 16'h0, 16'd360 ); 
      t_end   = $time;
      val     = ($urandom_range(FLOAT_INTERVAL,0)-HALF_INTERVAL);
      val_us  = -val;
      if( val < 0 )
        #(INTERVAL-(t_end-t_begin)+val_us);
      else
        #(INTERVAL-(t_end-t_begin)-val);        
    end
  end
  
  task send_scan( input [31:0] number, input [31:0] distance, input [47:0] c_time, input [15:0] angle1, input [15:0] angle2 );    
  
    crc.clear;

    uart8(             "S", crc );
    uart8(             "C", crc );
    uart8(   number[ 0+:8], crc );
    uart8(   number[ 8+:8], crc );
    uart8(   number[16+:8], crc );
    uart8(   number[24+:8], crc );
    uart8( distance[ 0+:8], crc );
    uart8( distance[ 8+:8], crc );
    uart8( distance[16+:8], crc );
    uart8( distance[24+:8], crc );
    uart8(   c_time[ 0+:8], crc );
    uart8(   c_time[ 8+:8], crc );
    uart8(   c_time[16+:8], crc );
    uart8(   c_time[24+:8], crc );
    uart8(   c_time[32+:8], crc );
    uart8(   c_time[40+:8], crc );
    uart8(    angle1[0+:8], crc );
    uart8(    angle1[8+:8], crc );
    uart8(    angle2[0+:8], crc );
    uart8(    angle2[8+:8], crc );
    crc_calc = crc.get_crc;
    if( IS_RANDOM_ER == "TRUE" )
      if( $urandom_range(1,0) == 1 ) 
        crc_calc = 16'hFFFF;
    uart8(  crc_calc[0+:8], crc );
    uart8(  crc_calc[8+:8], crc );
  endtask

  task automatic uart8 ( input [7:0] data, ref crc16 crc );
    reg [9:0] uart;
    crc.append_byte( data );
    uart = {1'b1, data, 1'b0};
    for( int i=0; i<10; i++ ) begin
      o_tx = uart[i];
      #400;
    end
  endtask

endmodule
