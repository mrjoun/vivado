`timescale 1ns/100ps

module clk_gen #(
  parameter real OUT_CLK = 33.333
)(
  output reg o_clk  = 1'b0,
  output reg o_rst  = 1'b1,
  output reg o_rstn = 1'b0
);

  localparam
    out_clk_t = 500/OUT_CLK;

  always #(out_clk_t) o_clk = ~o_clk;

  integer i;

  initial begin
    for( i=0; i<2500; i=i+1 )
      @(negedge o_clk);
    o_rst  = 1'b0;
    o_rstn = 1'b1;
  end

endmodule
