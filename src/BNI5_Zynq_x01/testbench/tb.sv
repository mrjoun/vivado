`timescale 1ns/100ps

module tb;

  //------------------------------------------------------
  // 
  //------------------------------------------------------
    wire srst_n;
    wire porb;
    
    assign porb = 1'b1;
  
  //------------------------------------------------------
  // Initial block
  //------------------------------------------------------
    initial begin
      bit [ 1:0] resp;
      bit [31:0] rddata;
      bit [31:0] addr;
      
      fpga1.arm_core_0.arm_inst.inst.pre_load_mem( 2'b10, 32'h0000_0000, 262144 );
      fpga1.arm_core_0.arm_inst.inst.set_debug_level_info( 1 );
      @(posedge srst_n);
      fpga1.arm_core_0.arm_inst.inst.gen_rst.fpga_soft_reset( '1 );
      fpga1.arm_core_0.arm_inst.inst.gen_rst.fpga_soft_reset( '0 );
      #100_000;
      /* INIT_CMD_CONTROLLER */
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h8000_0004, 4, 32'h0000_0000, resp ); //SET_ADDR_RD
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h8000_0008, 4, 32'h0020_0000, resp ); //SET_ADDR_WR
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h8000_0010, 4, 32'h001F_FFFF, resp ); //SET_SIZE  
      /* INIT_DATA_CONTROLLER */
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h4000_0004, 4, 32'h1FEF_FFFF, resp ); //SET_ADDR_RD
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h4000_0008, 4, 32'h2000_0000, resp ); //SET_ADDR_WR
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h4000_0010, 4, 32'h0010_0000, resp ); //SET_SIZE
      #100_000;
      addr = 32'h0;
      addr = send_data( addr, resp, {"F","1", 8'h0, 8'h0, 8'h4, 8'h0, 8'h1, 8'h0, 8'hFF, 8'hFF, "S", "W", 
                                                                                                8'h00, 8'h0, 
                                                                                                8'h00, 8'h0, 
                                                                                                "S", "A"} );      
      $display( "%m\tEND PROGRAM at %t", $time() );      
    end


  function send_data( bit [31:0] addr, bit [ 1:0] resp, bit [7:0] din8 [] );
    bit [31:0] size;
    int        mod;
    
    $display( "Len = %0d", din8.size() );
    size = din8.size();
    if( size % 8 != 0 ) begin
      din8 = new[size+(8-size%8)](din8);
      for( int i=size; i<din8.size(); i++ )
        din8[i] = 8'h0;
    end
    $display( "Len = %0d", din8.size() );
/*
    if( size % 2 != 0 ) begin
      fpga1.arm_core_0.arm_inst.inst.write_mem( 32'h0, addr, 4 );
      addr += 4;
      size++;
    end
    if( size/2/16 > 0 ) begin
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h8000_0020, 4, size/2/16, resp );
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h8000_0020, 4, (size/2)-(size/2/16), resp );            
    end
    else
      fpga1.arm_core_0.arm_inst.inst.write_data( 32'h8000_0020, 4, size/2, resp ); 
     din32.delete();          
    return addr;
*/    
  endfunction
  //------------------------------------------------------
  // 2 Clock generator
  //------------------------------------------------------
    wire clk_33;
    wire clk_200;
    
    clk_gen #(
      .OUT_CLK( 33.333 )
    ) clock_gen_33 (
      .o_clk ( clk_33 ),
      .o_rst (        ),
      .o_rstn( srst_n ) 
    );

    clk_gen #(
      .OUT_CLK( 200 )
    ) clock_gen_200 (
      .o_clk ( clk_200 ),
      .o_rst (         ),
      .o_rstn(         )
    );
  
  //------------------------------------------------------
  // Scan generator
  //------------------------------------------------------
    logic scan_line;
    
    scan_gen #(
      .START_DELAY   ( 5_300_000 ),
      .INTERVAL      ( 1_600_000 ),
      .FLOAT_INTERVAL(   100_000 ),
      .IS_RANDOM_ER  ( "TRUE"    )
    ) scan_gen_0 (
      .o_tx( scan_line )
    );

    BNI5_Zynq_x01 fpga1(
      .io_CPU_MIO  ( ),
      .io_DDR_CAS_n( ),
      .io_DDR_CKE  ( ),
      .io_DDR_Clk_n( ),
      .io_DDR_Clk  ( ),
      .io_DDR_CS_n ( ),
      .io_DDR_DRSTB( ),
      .io_DDR_ODT  ( ),
      .io_DDR_RAS_n( ),
      .io_DDR_WEB  ( ),
      .io_DDR_BA   ( ),
      .io_DDR_Addr ( ),
      .io_DDR_VRN  ( ),
      .io_DDR_VRP  ( ),
      .io_DDR_DM   ( ),
      .io_DDR_DQ   ( ),
      .io_DDR_DQS_n( ),
      .io_DDR_DQS  ( ),

      .io_PS_SRSTB( srst_n    ),
      .io_PS_CLK  ( clk_33    ),
      .io_PS_PORB ( porb      ),
      .GEN_200    ( clk_200   ),
      .SCAN       ( scan_line )
    );

endmodule
