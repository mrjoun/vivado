module CRC16_core( 
  input  [15:0] crc_in,
  input  [15:0] din, 
  output [15:0] crc_out
);

  wire [15:0] crc_int;
  wire [ 7:0] dint;

  assign crc_int[0]  =  din[0]     ^ din[1]     ^ din[2]     ^ din[3]     ^ din[4]    ^
                        din[5]     ^ din[6]     ^ din[7]     ^ crc_in[8]  ^ crc_in[9] ^
                        crc_in[10] ^ crc_in[11] ^ crc_in[12] ^ crc_in[13] ^
                        crc_in[14] ^ crc_in[15];

  assign crc_int[1]  =  din[0]     ^ din[1]     ^ din[2]     ^ din[3]     ^ din[4] ^ din[5] ^
                        din[6]     ^ crc_in[9]  ^ crc_in[10] ^ crc_in[11] ^
                        crc_in[12] ^ crc_in[13] ^ crc_in[14] ^ crc_in[15];
  assign crc_int[2]  =  din[6] ^ din[7] ^ crc_in[8]  ^ crc_in[9];
  assign crc_int[3]  =  din[5] ^ din[6] ^ crc_in[9]  ^ crc_in[10];
  assign crc_int[4]  =  din[4] ^ din[5] ^ crc_in[10] ^ crc_in[11];
  assign crc_int[5]  =  din[3] ^ din[4] ^ crc_in[11] ^ crc_in[12];
  assign crc_int[6]  =  din[2] ^ din[3] ^ crc_in[12] ^ crc_in[13];
  assign crc_int[7]  =  din[1] ^ din[2] ^ crc_in[13] ^ crc_in[14];
  assign crc_int[8]  =  din[0] ^ din[1] ^ crc_in[0]  ^ crc_in[14] ^ crc_in[15];
  assign crc_int[9]  =  din[0] ^ crc_in[1] ^ crc_in[15];
  assign crc_int[10] =  crc_in[2];
  assign crc_int[11] =  crc_in[3];
  assign crc_int[12] =  crc_in[4];
  assign crc_int[13] =  crc_in[5];
  assign crc_int[14] =  crc_in[6];
  assign crc_int[15] =  din[0]     ^ din[1]     ^ din[2]     ^ din[3]     ^ din[4]    ^ din[5] ^
                        din[6]     ^ din[7]     ^ crc_in[7]  ^ crc_in[8]  ^ crc_in[9] ^
                        crc_in[10] ^ crc_in[11] ^ crc_in[12] ^ crc_in[13] ^
                        crc_in[14] ^ crc_in[15];

  assign dint[7:0]   =  din[15:8];  

  assign crc_out[0]  =  dint[0]     ^ dint[1]     ^ dint[2]     ^ dint[3]     ^ dint[4]    ^
                        dint[5]     ^ dint[6]     ^ dint[7]     ^ crc_int[8]  ^ crc_int[9] ^
                        crc_int[10] ^ crc_int[11] ^ crc_int[12] ^ crc_int[13] ^
                        crc_int[14] ^ crc_int[15];
  assign crc_out[1]  =  dint[0]     ^ dint[1]     ^ dint[2]     ^ dint[3]     ^ dint[4] ^ dint[5] ^
                        dint[6]     ^ crc_int[9]  ^ crc_int[10] ^ crc_int[11] ^
                        crc_int[12] ^ crc_int[13] ^ crc_int[14] ^ crc_int[15];
  assign crc_out[2]  =  dint[6] ^ dint[7] ^ crc_int[8]  ^ crc_int[9];
  assign crc_out[3]  =  dint[5] ^ dint[6] ^ crc_int[9]  ^ crc_int[10];
  assign crc_out[4]  =  dint[4] ^ dint[5] ^ crc_int[10] ^ crc_int[11];
  assign crc_out[5]  =  dint[3] ^ dint[4] ^ crc_int[11] ^ crc_int[12];
  assign crc_out[6]  =  dint[2] ^ dint[3] ^ crc_int[12] ^ crc_int[13];
  assign crc_out[7]  =  dint[1] ^ dint[2] ^ crc_int[13] ^ crc_int[14];
  assign crc_out[8]  =  dint[0] ^ dint[1] ^ crc_int[0]  ^ crc_int[14] ^ crc_int[15];
  assign crc_out[9]  =  dint[0] ^ crc_int[1] ^ crc_int[15];
  assign crc_out[10] =  crc_int[2];
  assign crc_out[11] =  crc_int[3];
  assign crc_out[12] =  crc_int[4];
  assign crc_out[13] =  crc_int[5];
  assign crc_out[14] =  crc_int[6];
  assign crc_out[15] =  dint[0]     ^ dint[1]     ^ dint[2]     ^ dint[3]     ^ dint[4]    ^ dint[5] ^
                        dint[6]     ^ dint[7]     ^ crc_int[7]  ^ crc_int[8]  ^ crc_int[9] ^
                        crc_int[10] ^ crc_int[11] ^ crc_int[12] ^ crc_int[13] ^
                        crc_int[14] ^ crc_int[15];

endmodule
