module bram_gen #(
  parameter RAM_WIDTH = 18,
  parameter RAM_DEPTH = 1024,
  parameter RAM_PERFORMANCE = "HIGH_PERFORMANCE",
  parameter INIT_FILE = ""
)(
  input                         clka,
  input [clogb2(RAM_DEPTH-1)-1:0] addra,
  input [        RAM_WIDTH-1:0] dina,
  input                         wea,
  input                         ena,
  output [       RAM_WIDTH-1:0] douta,

  input                         clkb,
  input [clogb2(RAM_DEPTH-1)-1:0] addrb,
  input [        RAM_WIDTH-1:0] dinb,
  input                         web,
  input                         enb,
  output [       RAM_WIDTH-1:0] doutb
);

  reg [RAM_WIDTH-1:0] BRAM [RAM_DEPTH-1:0];
  reg [RAM_WIDTH-1:0] ram_data_a = {RAM_WIDTH{1'b0}};
  reg [RAM_WIDTH-1:0] ram_data_b = {RAM_WIDTH{1'b0}};
  
  generate
    if( INIT_FILE != "" ) begin: use_init_file
      initial 
        $readmemh( INIT_FILE, BRAM, 0, RAM_DEPTH-1 );
    end
    else begin: init_bram_to_zero
      integer ram_index;
      initial 
        for( ram_index=0; ram_index<RAM_DEPTH; ram_index=ram_index+1 )
          BRAM[ ram_index ] = {RAM_WIDTH{1'b0}};
    end
  endgenerate

  always @ (posedge clka)
    if( ena ) 
      if( wea )
        BRAM[ addra ] <= dina;
      else
        ram_data_a <= BRAM[ addra ];

  always @ (posedge clkb)
    if( enb ) 
      if( web )
        BRAM[ addrb ] <= dinb;
      else
        ram_data_b <= BRAM[ addrb ];

  generate 
    if( RAM_PERFORMANCE == "LOW_LATENCY" ) begin: no_output_register
    
      assign douta = ram_data_a;
      assign doutb = ram_data_b;
    
    end 
    else begin: output_register
      
      reg [RAM_WIDTH-1:0] douta_reg = {RAM_WIDTH{1'b0}};
      reg [RAM_WIDTH-1:0] doutb_reg = {RAM_WIDTH{1'b0}};

      always @ (posedge clka)
        douta_reg <= ram_data_a;

      always @ (posedge clkb)
        doutb_reg <= ram_data_b;

      assign douta = douta_reg;
      assign doutb = doutb_reg;

    end
  endgenerate

  function integer clogb2;
    input integer depth;
    begin
      for( clogb2=0; depth>0; clogb2=clogb2+1 ) 
        depth = depth >> 1;
    end
  endfunction

endmodule
