module width_conv_x2 #(
  parameter WD = 9
)(
  i_clk,
  i_srst,
  i_din,
  i_wr,
  o_dout,
  o_wr
);

  //---------------------------------------------------------------
  // PORT Declaration
  //---------------------------------------------------------------
    localparam
      D0 = WD-1,    //8
      D1 = (D0<<1); //16
    
    input  wire         i_clk;
    input  wire         i_srst;
    input  wire [WD-1:0] i_din;
    input  wire          i_wr;
    output reg  [  D1:0] o_dout;
    output reg           o_wr;

  //---------------------------------------------------------------
  // Logic
  //--------------------------------------------------------------- 
    reg is_even;

    always @ (posedge i_clk)
      if( i_srst ) begin
        o_wr    <= 1'h0;
        is_even <= 1'h0;
        o_dout  <= {(D1+1){1'b0}};
      end
      else begin
        o_wr <= 1'h0;
        if( i_wr ) begin
          o_wr       <= is_even | i_din[WD-1];
          o_dout[D1] <= i_din[WD-1];
          case( is_even )
            1'b0: begin
              is_even         <= ~i_din[WD-1];
              o_dout[D0-1: 0] <= i_din[D0-1:0];
              o_dout[D1-1:D0] <= {(WD-1){1'b0}};
            end
            1'b1: begin
              is_even         <= 1'h0;
              o_dout[D1-1:D0] <= i_din[D0-1:0];
            end
          endcase
        end // i_wr
      end // else

endmodule
