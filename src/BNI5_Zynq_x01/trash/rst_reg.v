module rst_reg #(
  parameter INIT = 1'b0
)(
  input  wire i_clk,
  input  wire i_rst,
  output reg  o_rst = INIT
);

  (* ASYNC_REG="TRUE" *) reg [1:0] sreg = {INIT, INIT};

  always @ (posedge i_clk) begin
    o_rst <= sreg[1];
    sreg  <= {sreg[0], i_rst};
  end

endmodule
