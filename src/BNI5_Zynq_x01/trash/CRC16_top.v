module CRC16_top( 
  input i_clk,
  input i_srst,
   
  input i_clr, 
  
  input  [15:0] i_data, 
  input         i_wr,
  output [15:0] o_crc
);

  reg  [15:0] curent_crc_rx;
  wire [15:0] next_crc_rx;

  assign o_crc = ~curent_crc_rx;
  
  always @(posedge i_clk)
    if( i_srst | i_clr )
      curent_crc_rx <= 16'hFFFF;
    else
      if (i_wr)
        curent_crc_rx <= next_crc_rx;

  CRC16_core crc_core( 
   .crc_in  ( curent_crc_rx ), 
   .din     ( i_data        ), 
   .crc_out ( next_crc_rx   )
  ); 

endmodule
