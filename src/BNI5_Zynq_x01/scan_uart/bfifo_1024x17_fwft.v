module bfifo_1024x17_fwft(
  input        i_clk,
  input        i_rst,

  input  [16:0] i_din,
  input         i_wr,
  output        o_full,

  output [16:0] o_dout,
  input         i_rd,
  output reg    o_empty
);

  wire [31:0] fdin;
  wire [31:0] fdout;
  wire [ 3:0] fdip;
  wire [ 3:0] fdop;
  wire        rden;
  wire        fempty;
  reg         empty_q;

  assign o_dout = { fdop[0], fdout[15:0] };
  assign fdin   = { 16'h0, i_din[15:0] };
  assign fdip   = {  3'h0, i_din[16]   };
  assign rden   = i_rd | o_empty & empty_q & ~fempty;

  always @ (posedge i_clk, posedge i_rst )
    if( i_rst )
      empty_q <= 1'h1;
    else
      empty_q <= fempty;

  always @ (posedge i_clk, posedge i_rst )
    if( i_rst ) 
      o_empty <= 1'h1;
    else
      o_empty <= (fempty & i_rd) | (~empty_q & ~i_rd & o_empty) | (fempty & ~i_rd & o_empty );

  FIFO18E1 #(
    .ALMOST_EMPTY_OFFSET    ( 13'h3     ),
    .ALMOST_FULL_OFFSET     ( 13'h2     ),
    .DATA_WIDTH             ( 18        ),
    .DO_REG                 ( 0         ), 
    .EN_SYN                 ( "TRUE"    ),
    .FIFO_MODE              ( "FIFO18"  ),
    .FIRST_WORD_FALL_THROUGH( "FALSE"   ),
    .INIT                   ( 36'h00000 ),
    .SIM_DEVICE             ( "7SERIES" ),
    .SRVAL                  ( 36'h00000 )
  ) inst (
    .WRCLK      ( i_clk  ),
    .DI         ( fdin   ),
    .DIP        ( fdip   ),
    .WREN       ( i_wr   ),
    .FULL       ( o_full ),
    .ALMOSTFULL ( ),
    .WRCOUNT    ( ),
    .WRERR      ( ),
    .RDCLK      ( i_clk  ),
    .DO         ( fdout  ),
    .DOP        ( fdop   ),
    .RDEN       ( rden   ),
    .EMPTY      ( fempty ),
    .ALMOSTEMPTY( ),
    .RDCOUNT    ( ),
    .RDERR      ( ),
    .REGCE      ( 1'b0  ),
    .RSTREG     ( 1'b0  ),
    .RST        ( i_rst )
  );

endmodule
