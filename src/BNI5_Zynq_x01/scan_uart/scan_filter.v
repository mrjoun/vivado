module scan_filter #(
  parameter INIT = 1'b1
)(
  input      i_clk,
  input      i_srst,

  input      i_rx,
  output reg o_rx = INIT
);

  wire [8:0] rx_t;
  reg  [2:0] sum;
  
  assign rx_t[0] = i_rx;

  genvar i;
  generate 
    for( i=0; i<8; i=i+1 ) begin
      if( i == 0 ) begin
        (* IOB="true" *) FD #( 
          .INIT( INIT )
        ) trig (
          .C( i_clk    ),
          .D( rx_t[i]  ),
          .Q( rx_t[i+1])
        );
      end
      else begin
        FD #(
          .INIT( INIT )
        ) trig (
          .C( i_clk     ),
          .D( rx_t[i]   ),
          .Q( rx_t[i+1] )
        );
      end
    end
  endgenerate

  always @ (posedge i_clk)
    sum <= rx_t[8] + rx_t[7] + rx_t[6] + rx_t[5] + 
           rx_t[4] + rx_t[3] + rx_t[2];
  
  always @ (posedge i_clk)
    if( i_srst ) 
      o_rx <= INIT; 
    else
      o_rx <= sum[2];

endmodule
