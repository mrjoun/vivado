module scan_log #(
  parameter LOG_NUM = 3
)(
  input i_clk,
  input i_srst,

  input      [16:0] i_din,
  output reg        o_rd,
  input             i_empty,

  input             i_lg_clk,
  input             i_lg_wren,
  input      [ 3:0] i_lg_num,
  input      [ 9:0] i_lg_addr,
  input      [15:0] i_lg_din,
  input             i_lg_wr,
  output reg [15:0] o_lg_dout,
  output            o_lg_rdy
);
 
  //------------------------------------------------------------------------
  // In logic
  //------------------------------------------------------------------------
    localparam
      st_IDLE = 0,
      st_COPY = 1,
      st_CNTR = 2,
      st_ENDL = 3,
      st_W8   = 4;
    
    reg  [ 9:0] slv_lg_addr;
    reg  [15:0] slv_lg_din;
    reg         slv_lg_wr;
    wire [15:0] slv_lg_dout;

    reg  [ 2:0] state;

    reg  [ 1:0] timer;
    reg  [ 1:0] timer_next;
    reg         timer_set;
    reg         timer_timeout;

    reg         crc_ok, crc_er;

    wire [ 9:0] bufr;
    
    assign bufr = slv_lg_addr - 10'h1;

    always @* begin
      timer_timeout = (timer == 2'b00);
      if( timer_set )
        timer_next = 2'b11;
      else
        timer_next = timer_timeout ? 2'b0 : timer - 2'b1; 
    end

    always @ (posedge i_clk)
      if( i_srst ) begin
        state       <= st_IDLE;
        o_rd        <= 1'h0;
        slv_lg_wr   <= 1'h0;
        timer_set   <= 1'h0;
        timer       <= 2'h0;
        slv_lg_addr <= 10'h0;
        slv_lg_din  <= 16'h0;
      end
      else begin
        timer     <= timer_next;
        o_rd      <= 1'h0;
        slv_lg_wr <= 1'h0;
        timer_set <= 1'h0;
        case( state )
          st_IDLE: begin
            if( ~i_empty & i_lg_wren ) begin
              state       <= st_COPY;
              o_rd        <= 1'h1;
              slv_lg_addr <= 10'h1;
            end
          end
          st_COPY: begin
            slv_lg_din <= i_din[15:0];
            slv_lg_wr  <= o_rd;
            if( o_rd ) 
              slv_lg_addr <= slv_lg_addr + 10'h1;
            if( ~i_empty & o_rd & i_din[15] )
              state <= st_CNTR;
            else
              o_rd <= ~i_empty & ~o_rd;
          end
          st_CNTR: 
            if( crc_ok | crc_er ) begin
              slv_lg_addr <= 10'h1;
              slv_lg_wr   <=  1'h1;
              if( crc_ok )
                slv_lg_din <= { "W", bufr[7:0] };
              else
                slv_lg_din <= { "E", bufr[7:0] };
              state       <= st_ENDL;
              timer_set   <=  1'h1;
            end
          st_ENDL: begin
            slv_lg_addr <= 10'h0;
            slv_lg_wr   <=  1'h1;
            slv_lg_din  <= 16'hFFFF;
            state       <= st_W8;
          end
          st_W8:
            if( (slv_lg_dout == 16'h0000) & timer_timeout)
              state <= st_IDLE;
        endcase
      end

  //------------------------------------------------------------------------
  // out logic
  //------------------------------------------------------------------------
    wire        is_log_select;
    wire [15:0] log_dout;

    assign is_log_select = (i_lg_num == LOG_NUM);
    assign o_lg_rdy      = (log_dout == 16'hFFFF) && (~|i_lg_addr);

    always @ (posedge i_lg_clk)
      if( is_log_select ) 
        o_lg_dout <= log_dout;
      else
        o_lg_dout <= 16'h0;

  //------------------------------------------------------------------------
  // CRC
  //------------------------------------------------------------------------
    reg  [15:0] crc_din;
    wire [15:0] crc_dout;
    reg         crc_wr;
    reg         crc_clr;

    always @ (posedge i_clk)
      if( i_srst ) begin
        crc_wr  <=  1'h0;
        crc_clr <=  1'h0;
        crc_din <= 16'h0;
      end
      else begin
        crc_wr  <= o_rd & ~i_din[16];
        crc_clr <= o_rd &  i_din[16];
        crc_din <= i_din[15:0];
      end

    always @ (posedge i_clk)
      if( (i_srst) | (o_rd & ~i_din[16]) ) begin
        crc_ok <= 1'h0;
        crc_er <= 1'h0;
      end
      else
        if( crc_clr ) begin
          crc_ok <= (crc_dout == crc_din);
          crc_er <= (crc_dout != crc_din);
        end

    CRC16_top crc(
      .i_clk ( i_clk    ),
      .i_srst( i_srst   ),
      .i_clr ( crc_clr  ),
      .i_data( crc_din  ),
      .i_wr  ( crc_wr   ),
      .o_crc ( crc_dout )
    );
 
  //------------------------------------------------------------------------
  // Instance's
  //------------------------------------------------------------------------
    bram_gen #(
      .RAM_WIDTH      ( 16            ),
      .RAM_DEPTH      ( 1024          ),
      .RAM_PERFORMANCE( "LOW_LATENCY" ),
      .INIT_FILE      ( ""            )
    ) log_mem_0 (
      .clka ( i_clk                   ),
      .addra( slv_lg_addr             ),
      .dina ( slv_lg_din              ),
      .wea  ( slv_lg_wr               ),
      .ena  ( 1'b1                    ),
      .douta( slv_lg_dout             ),
      .clkb ( i_lg_clk                ),
      .addrb( i_lg_addr               ),
      .dinb ( i_lg_din                ),
      .web  ( is_log_select & i_lg_wr ),
      .enb  ( 1'b1                    ),
      .doutb( log_dout                )
    );

endmodule
