module scan_uart8_rx(
  input i_clk,
  input i_srst,

  input i_rx,
  input i_en,

  output reg [7:0] o_data,
  output reg       o_wr,
  output reg       o_last
);
  
  localparam
    DIVISOR      = 16,
    PART         = DIVISOR >> 2,     //4
    FIRST_PART   = DIVISOR - 3*PART, //4
    PKTEND       = DIVISOR * 7,
    st_IDLE      = 0,
    st_RX_DATA_1 = 1,
    st_RX_DATA_2 = 2,
    st_RX_DATA_3 = 3,
    st_STOP_BIT  = 4,
    st_PKT_END   = 5;

  reg [7:0] shift_reg;
  reg [3:0] zero_cnt;
  reg [6:0] clk_cnt;
  reg [2:0] bit_cnt;
  reg [2:0] state;
  reg       is_first;

  reg       shift_dat;
  reg       clr_bit;
  reg       inc_bit;
  reg       clr_zero;
  reg       run_zero;
  reg       set_cntr_2;
  reg       set_cntr_1;
  reg       set_cntr_0;
  reg [4:0] status;

  always @ (posedge i_clk)
    if( i_srst ) begin
      o_wr       <= 1'h0;
      o_last     <= 1'h0;
      shift_dat  <= 1'h0;
      clr_bit    <= 1'h0;
      inc_bit    <= 1'h0;
      clr_zero   <= 1'h0;
      run_zero   <= 1'h0;
      set_cntr_0 <= 1'h0;
      set_cntr_1 <= 1'h0;
      set_cntr_2 <= 1'h0;
      is_first   <= 1'h0;
      state      <= st_IDLE;
    end
    else begin
      o_wr       <= 1'h0;
      o_last     <= 1'h0;
      shift_dat  <= 1'h0;
      clr_bit    <= 1'h0;
      inc_bit    <= 1'h0;
      clr_zero   <= 1'h0;
      set_cntr_0 <= 1'h0;
      set_cntr_1 <= 1'h0;
      set_cntr_2 <= 1'h0;
      case( state ) 
        st_IDLE: begin
          set_cntr_2 <= 1'h1;
          clr_bit    <= 1'h1;
          is_first   <= 1'h1;
          if( (i_rx == 1'b0) && (i_en == 1'b1) )
            state    <= st_RX_DATA_1;
        end
        st_RX_DATA_1:
          if( status[0] ) begin
            run_zero <= 1'h1;
            state    <= st_RX_DATA_2;
          end
          else
            clr_zero <= 1'h1;
        st_RX_DATA_2:
          if( status[1] ) begin
            run_zero <= 1'h0;
            state    <= st_RX_DATA_3;
          end
        st_RX_DATA_3: begin
          set_cntr_1 <= status[2] & ~(&bit_cnt);
          set_cntr_0 <= status[2] & (&bit_cnt);
          clr_zero   <= status[2];
          shift_dat  <= status[2];

          if( clr_zero ) begin
            if( is_first ) begin
              if( zero_cnt[2] | zero_cnt[3] ) begin
                is_first <= 1'h0;
                state    <= st_RX_DATA_1;
              end
              else
                state   <= st_IDLE;
            end
            else
              if( &bit_cnt ) 
                state   <= st_STOP_BIT;
              else begin
                state   <= st_RX_DATA_1;
                inc_bit <= 1'h1;
              end
          end
        end
        st_STOP_BIT: begin
          set_cntr_0 <= status[3];
          if( status[3] ) 
            state <= st_PKT_END;
        end
        st_PKT_END: begin
          clr_bit <= 1'h1;
          is_first <= 1'h1;
          if( (i_rx == 1'b0) && (i_en == 1'b1) ) begin
            o_wr       <= 1'h1;
            state      <= st_RX_DATA_1;
            set_cntr_2 <= 1'h1;
          end
          else
            if( status[4] ) begin
              state  <= st_IDLE;
              o_wr   <= 1'h1;
              o_last <= 1'h1;
            end
        end
      endcase
    end

  always @ (posedge i_clk)
    if( shift_dat )
      if( zero_cnt[3] | zero_cnt[2] ) 
        shift_reg <= {1'b0, shift_reg[7:1]};
      else
        shift_reg <= {1'b1, shift_reg[7:1]};

  always @ (posedge i_clk)
    if( clr_bit )
      bit_cnt <= 4'h0;
    else
      if( inc_bit ) 
        bit_cnt <= bit_cnt + 4'h1;

  always @ (posedge i_clk)
    if( set_cntr_0 ) 
      clk_cnt <= 7'h0;
    else
      if( set_cntr_1 ) 
        clk_cnt <= 7'h1;
      else
        if( set_cntr_2 ) 
          clk_cnt <= 7'h2;
        else 
          clk_cnt <= clk_cnt + 7'h1;

  always @ (posedge i_clk) begin
    status[0] <= (clk_cnt == FIRST_PART-1);
    status[1] <= (clk_cnt == (FIRST_PART+(PART<<1)-1) );
    status[2] <= (clk_cnt == DIVISOR-2);
    status[3] <= (clk_cnt == (DIVISOR >> 1)-1);
    status[4] <= (clk_cnt == PKTEND-1 );
  end

  always @ (posedge i_clk)
    if( clr_zero ) zero_cnt <= 5'h0;
    else
      if( (run_zero == 1'b1) && (i_rx == 1'b0) ) 
        zero_cnt <= zero_cnt + 5'h1;

  always @ (posedge i_clk)
    o_data <= shift_reg;

endmodule
