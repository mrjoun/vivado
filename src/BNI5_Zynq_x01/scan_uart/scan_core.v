module scan_core #(
  parameter LOG_NUM = 3
)(
  input i_clk,
  input i_srst,

  input i_rx,
  input i_en,

  //AXI4-Stream to MUX
  output reg [16:0] o_tdata,
  output reg        o_tvalid,
  input             i_tready,

  //Log-bus
  input         i_lg_clk,
  input         i_lg_wren,
  input  [ 3:0] i_lg_num,
  input  [ 9:0] i_lg_addr,
  input  [15:0] i_lg_din,
  input         i_lg_wr,
  output [15:0] o_lg_dout,
  output        o_lg_rdy
);

  //-------------------------------------------------------------
  // Signal declaration
  //-------------------------------------------------------------
    wire        rx_filt;
    wire [ 8:0] uart_x9_dout;
    wire        uart_x9_wr;
    wire [16:0] uart_x17_dout;
    wire        uart_x17_wr;
    wire [16:0] fifo_strm_dout , fifo_log_dout;
    wire        fifo_strm_rd   , fifo_log_rd;
    wire        fifo_strm_empty, fifo_log_empty;

  //-------------------------------------------------------------
  // Logic axi4-stream
  //-------------------------------------------------------------
    assign fifo_strm_rd = o_tvalid;
    
    always @ (posedge i_clk)
      o_tdata <= fifo_strm_dout;
    
    always @ (posedge i_clk)
      if( i_srst )
        o_tvalid <= 1'h0;
      else
        o_tvalid <= i_tready & ~o_tvalid & ~fifo_strm_empty;
  
  //-------------------------------------------------------------
  // Instance's
  //-------------------------------------------------------------
    scan_filter #(
      .INIT  ( 1'b1 )
    ) scan_filter_0 (
      .i_clk ( i_clk   ),
      .i_srst( i_srst  ),
      .i_rx  ( i_rx    ),
      .o_rx  ( rx_filt )
    );

    scan_uart8_rx scan_uart8_rx_0 (
      .i_clk  ( i_clk             ),
      .i_srst ( i_srst            ),
      .i_rx   ( rx_filt           ),
      .i_en   ( i_en              ),
      .o_data ( uart_x9_dout[7:0] ),
      .o_wr   ( uart_x9_wr        ),
      .o_last ( uart_x9_dout[8]   )
    );

    width_conv_x2 #(
      .WD( 9 )
    ) width_conv_x2_0 (
      .i_clk ( i_clk         ),
      .i_srst( i_srst        ),
      .i_din ( uart_x9_dout  ),
      .i_wr  ( uart_x9_wr    ),
      .o_dout( uart_x17_dout ),
      .o_wr  ( uart_x17_wr   )
    );
    
    bfifo_1024x17_fwft fifo_uart_stream_0 (
      .i_clk  ( i_clk           ),
      .i_rst  ( i_srst          ),
      .i_din  ( uart_x17_dout   ),
      .i_wr   ( uart_x17_wr     ),
      .o_full (                 ),
      .o_dout ( fifo_strm_dout  ),
      .i_rd   ( fifo_strm_rd    ),
      .o_empty( fifo_strm_empty )
    );

    bfifo_1024x17_fwft fifo_uart_log_0 (
      .i_clk  ( i_clk          ),
      .i_rst  ( i_srst         ),
      .i_din  ( uart_x17_dout  ),
      .i_wr   ( uart_x17_wr    ),
      .o_full (                ),
      .o_dout ( fifo_log_dout  ),
      .i_rd   ( fifo_log_rd    ),
      .o_empty( fifo_log_empty )
    );

    scan_log #(
      .LOG_NUM  ( LOG_NUM )
    ) scan_log_0 (
      .i_clk    ( i_clk          ),
      .i_srst   ( i_srst         ),
      .i_din    ( fifo_log_dout  ),
      .o_rd     ( fifo_log_rd    ),
      .i_empty  ( fifo_log_empty ),
      .i_lg_clk ( i_lg_clk       ),
      .i_lg_wren( i_lg_wren      ),
      .i_lg_num ( i_lg_num       ),
      .i_lg_addr( i_lg_addr      ),
      .i_lg_din ( i_lg_din       ),
      .i_lg_wr  ( i_lg_wr        ),
      .o_lg_dout( o_lg_dout      ),
      .o_lg_rdy ( o_lg_rdy       )
    );

endmodule
