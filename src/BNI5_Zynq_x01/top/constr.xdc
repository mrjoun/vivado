set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]

create_clock -period 8.000 -name MGTREFCLK_125_P -waveform {0.000 4.000} [get_ports MGTREFCLK_125_P]
create_clock -period 4.706 -name MGTREFCLK_212_P -waveform {0.000 2.353} [get_ports MGTREFCLK_212_P]
