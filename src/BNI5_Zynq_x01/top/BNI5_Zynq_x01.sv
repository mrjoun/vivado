module BNI5_Zynq_x01 (
	// ARM CPU PINS
    inout [53:0] io_CPU_MIO,
    inout        io_DDR_CAS_n,
    inout        io_DDR_CKE,
    inout        io_DDR_Clk_n,
    inout        io_DDR_Clk,
    inout        io_DDR_CS_n,
    inout        io_DDR_DRSTB,
    inout        io_DDR_ODT,
	  inout        io_DDR_RAS_n,
    inout        io_DDR_WEB,
    inout [ 2:0] io_DDR_BA,
    inout [14:0] io_DDR_Addr,
    inout        io_DDR_VRN,
    inout        io_DDR_VRP,
    inout [ 3:0] io_DDR_DM,
    inout [31:0] io_DDR_DQ,
    inout [ 3:0] io_DDR_DQS_n,
    inout [ 3:0] io_DDR_DQS,
    inout        io_PS_SRSTB,
    inout        io_PS_CLK,
    inout        io_PS_PORB,

  (* LOC="V15",  IOSTANDARD="LVTTL" *) inout FM2_CS,
	(* LOC="U14",  IOSTANDARD="LVTTL" *) inout FM2_SCK,
	(* LOC="V14",  IOSTANDARD="LVTTL" *) inout FM2_SI,
	(* LOC="U13",  IOSTANDARD="LVTTL" *) inout FM2_SO,

	(* LOC="F7",   IOSTANDARD="LVTTL" *) inout FPGA_D0,
	(* LOC="E7",   IOSTANDARD="LVTTL" *) inout FPGA_D1,
	(* LOC="C8",   IOSTANDARD="LVTTL" *) inout FPGA_D2,
	(* LOC="D6",   IOSTANDARD="LVTTL" *) inout FPGA_D3,
	(* LOC="E8",   IOSTANDARD="LVTTL" *) inout FPGA_D4,
	(* LOC="D8",   IOSTANDARD="LVTTL" *) inout FPGA_D5,
	(* LOC="G4",   IOSTANDARD="LVTTL" *) inout FPGA_D6,
	(* LOC="E5",   IOSTANDARD="LVTTL" *) inout FPGA_D7,
	(* LOC="F6",   IOSTANDARD="LVTTL" *) inout FPGA_D8,
	(* LOC="D7",   IOSTANDARD="LVTTL" *) inout FPGA_D9,
	(* LOC="B8",   IOSTANDARD="LVTTL" *) inout FPGA_D10,
	(* LOC="B7",   IOSTANDARD="LVTTL" *) inout FPGA_D11,
	(* LOC="B6",   IOSTANDARD="LVTTL" *) inout FPGA_D12,
	(* LOC="A7",   IOSTANDARD="LVTTL" *) inout FPGA_D13,
	(* LOC="A6",   IOSTANDARD="LVTTL" *) inout FPGA_D14,
	(* LOC="A5",   IOSTANDARD="LVTTL" *) inout FPGA_D15,
	(* LOC="A4",   IOSTANDARD="LVTTL" *) inout FPGA_D16,
	(* LOC="C6",   IOSTANDARD="LVTTL" *) inout FPGA_D17,
	(* LOC="C5",   IOSTANDARD="LVTTL" *) inout FPGA_D18,
	(* LOC="D5",   IOSTANDARD="LVTTL" *) inout FPGA_D19,
	(* LOC="C4",   IOSTANDARD="LVTTL" *) inout FPGA_D20,
	(* LOC="B4",   IOSTANDARD="LVTTL" *) inout FPGA_D21,
	(* LOC="B3",   IOSTANDARD="LVTTL" *) inout FPGA_D22,
	(* LOC="D3",   IOSTANDARD="LVTTL" *) inout FPGA_D23,
	(* LOC="C3",   IOSTANDARD="LVTTL" *) inout FPGA_D24,
	(* LOC="A2",   IOSTANDARD="LVTTL" *) inout FPGA_D25,
	(* LOC="A1",   IOSTANDARD="LVTTL" *) inout FPGA_D26,
	(* LOC="D1",   IOSTANDARD="LVTTL" *) inout FPGA_D27,
	(* LOC="C1",   IOSTANDARD="LVTTL" *) inout FPGA_D28,
	(* LOC="E2",   IOSTANDARD="LVTTL" *) inout FPGA_D29,
	(* LOC="D2",   IOSTANDARD="LVTTL" *) inout FPGA_D30,
	(* LOC="B2",   IOSTANDARD="LVTTL" *) inout FPGA_D31,
	(* LOC="E4",   IOSTANDARD="LVTTL" *) inout FPGA_D32,
	(* LOC="E3",   IOSTANDARD="LVTTL" *) inout FPGA_D33,
	(* LOC="G2",   IOSTANDARD="LVTTL" *) inout FPGA_D34,
	(* LOC="F1",   IOSTANDARD="LVTTL" *) inout FPGA_D35,
	(* LOC="B1",   IOSTANDARD="LVTTL" *) inout FPGA_D36,
	(* LOC="F5",   IOSTANDARD="LVTTL" *) inout FPGA_D37,
	(* LOC="H3",   IOSTANDARD="LVTTL" *) inout FPGA_D38,
	(* LOC="H4",   IOSTANDARD="LVTTL" *) inout FPGA_D39,
	(* LOC="F4",   IOSTANDARD="LVTTL" *) inout FPGA_D40,
	(* LOC="H5",   IOSTANDARD="LVTTL" *) inout FPGA_D41,
	
  (* LOC="L5",   IOSTANDARD="LVTTL" *) input GEN_200,
	
  (* LOC="V9",   IOSTANDARD="LVTTL" *) inout MGTREFCLK_125_N,
	(* LOC="U9",   IOSTANDARD="LVTTL" *) inout MGTREFCLK_125_P,
	(* LOC="V5",   IOSTANDARD="LVTTL" *) inout MGTREFCLK_212_N,
	(* LOC="U5",   IOSTANDARD="LVTTL" *) inout MGTREFCLK_212_P,
	
  (* LOC="U16",  IOSTANDARD="LVTTL" *) input SCAN,

	(* LOC="Y18",  IOSTANDARD="LVTTL" *) inout SFP1_RX_LOS,
	(* LOC="AB7",  IOSTANDARD="LVTTL" *) inout SFP1_RX_N,
	(* LOC="AA7",  IOSTANDARD="LVTTL" *) inout SFP1_RX_P,
	(* LOC="W17",  IOSTANDARD="LVTTL" *) inout SFP1_TX_DISABLE,
	(* LOC="Y19",  IOSTANDARD="LVTTL" *) inout SFP1_TX_FAULT,
	(* LOC="AB3",  IOSTANDARD="LVTTL" *) inout SFP1_TX_N,
	(* LOC="AA3",  IOSTANDARD="LVTTL" *) inout SFP1_TX_P,
	(* LOC="V19",  IOSTANDARD="LVTTL" *) inout SFP2_RX_LOS,
	(* LOC="AB9",  IOSTANDARD="LVTTL" *) inout SFP2_RX_N,
	(* LOC="AA9",  IOSTANDARD="LVTTL" *) inout SFP2_RX_P,
	(* LOC="U19",  IOSTANDARD="LVTTL" *) inout SFP2_TX_DISABLE,
	(* LOC="W18",  IOSTANDARD="LVTTL" *) inout SFP2_TX_FAULT,
	(* LOC="AB5",  IOSTANDARD="LVTTL" *) inout SFP2_TX_N,
	(* LOC="AA5",  IOSTANDARD="LVTTL" *) inout SFP2_TX_P,

	(* LOC="K3",   IOSTANDARD="LVTTL" *) inout SRAM_ADSC,
	(* LOC="L2",   IOSTANDARD="LVTTL" *) inout SRAM_ADSP,
	(* LOC="K2",   IOSTANDARD="LVTTL" *) inout SRAM_ADV,
	(* LOC="M1",   IOSTANDARD="LVTTL" *) inout SRAM_A0,
	(* LOC="M2",   IOSTANDARD="LVTTL" *) inout SRAM_A1,
	(* LOC="N1",   IOSTANDARD="LVTTL" *) inout SRAM_A2,
	(* LOC="M3",   IOSTANDARD="LVTTL" *) inout SRAM_A3,
	(* LOC="N3",   IOSTANDARD="LVTTL" *) inout SRAM_A4,
	(* LOC="R2",   IOSTANDARD="LVTTL" *) inout SRAM_A5,
	(* LOC="P2",   IOSTANDARD="LVTTL" *) inout SRAM_A6,
	(* LOC="P3",   IOSTANDARD="LVTTL" *) inout SRAM_A7,
	(* LOC="N4",   IOSTANDARD="LVTTL" *) inout SRAM_A8,
	(* LOC="N5",   IOSTANDARD="LVTTL" *) inout SRAM_A9,
	(* LOC="R7",   IOSTANDARD="LVTTL" *) inout SRAM_A00,
	(* LOC="P5",   IOSTANDARD="LVTTL" *) inout SRAM_A01,
	(* LOC="N6",   IOSTANDARD="LVTTL" *) inout SRAM_A10,
	(* LOC="M6",   IOSTANDARD="LVTTL" *) inout SRAM_A11,
	(* LOC="K5",   IOSTANDARD="LVTTL" *) inout SRAM_A12,
	(* LOC="J1",   IOSTANDARD="LVTTL" *) inout SRAM_A13,
	(* LOC="L1",   IOSTANDARD="LVTTL" *) inout SRAM_A14,
	(* LOC="J3",   IOSTANDARD="LVTTL" *) inout SRAM_A15,
	(* LOC="J7",   IOSTANDARD="LVTTL" *) inout SRAM_A16,
	(* LOC="J2",   IOSTANDARD="LVTTL" *) inout SRAM_A17,
	(* LOC="J6",   IOSTANDARD="LVTTL" *) inout SRAM_A18,
	(* LOC="J5",   IOSTANDARD="LVTTL" *) inout SRAM_A19,
	(* LOC="L4",   IOSTANDARD="LVTTL" *) inout SRAM_BWE,
	(* LOC="T2",   IOSTANDARD="LVTTL" *) inout SRAM_CLK,
	(* LOC="P8",   IOSTANDARD="LVTTL" *) inout SRAM_D0,
	(* LOC="M8",   IOSTANDARD="LVTTL" *) inout SRAM_D1,
	(* LOC="L7",   IOSTANDARD="LVTTL" *) inout SRAM_D2,
	(* LOC="J8",   IOSTANDARD="LVTTL" *) inout SRAM_D3,
	(* LOC="U2",   IOSTANDARD="LVTTL" *) inout SRAM_D4,
	(* LOC="R3",   IOSTANDARD="LVTTL" *) inout SRAM_D5,
	(* LOC="R5",   IOSTANDARD="LVTTL" *) inout SRAM_D6,
	(* LOC="M7",   IOSTANDARD="LVTTL" *) inout SRAM_D7,
	(* LOC="U1",   IOSTANDARD="LVTTL" *) inout SRAM_D8,
	(* LOC="R8",   IOSTANDARD="LVTTL" *) inout SRAM_D9,
	(* LOC="N8",   IOSTANDARD="LVTTL" *) inout SRAM_D10,
	(* LOC="L6",   IOSTANDARD="LVTTL" *) inout SRAM_D11,
	(* LOC="K8",   IOSTANDARD="LVTTL" *) inout SRAM_D12,
	(* LOC="P1",   IOSTANDARD="LVTTL" *) inout SRAM_D13,
	(* LOC="T1",   IOSTANDARD="LVTTL" *) inout SRAM_D14,
	(* LOC="R4",   IOSTANDARD="LVTTL" *) inout SRAM_D15,
	(* LOC="P6",   IOSTANDARD="LVTTL" *) inout SRAM_D16,
	(* LOC="H8",   IOSTANDARD="LVTTL" *) inout SRAM_D17,
	(* LOC="M4",   IOSTANDARD="LVTTL" *) inout SRAM_GW,
	(* LOC="K4",   IOSTANDARD="LVTTL" *) inout SRAM_OE,
	(* LOC="P7",   IOSTANDARD="LVTTL" *) inout SRAM_ZZ,
	
  (* LOC="Y8",   IOSTANDARD="LVTTL" *) inout RX1N,
	(* LOC="W8",   IOSTANDARD="LVTTL" *) inout RX1P,
	(* LOC="Y4",   IOSTANDARD="LVTTL" *) inout TX1N,
	(* LOC="W4",   IOSTANDARD="LVTTL" *) inout TX1P,
	(* LOC="Y6",   IOSTANDARD="LVTTL" *) inout RX3N,
	(* LOC="W6",   IOSTANDARD="LVTTL" *) inout RX3P,
	(* LOC="Y2",   IOSTANDARD="LVTTL" *) inout TX3N,
	(* LOC="W2",   IOSTANDARD="LVTTL" *) inout TX3P
);
  
  //---------------------------------------------------------------
  // Clock and Reset
  //---------------------------------------------------------------
    wire arm_clk_40;
    wire arm_rst_40;
    wire pl_clk_50;
    wire pl_clk_100;
    wire pl_clk_200;
    wire pl_rstn_200;
    wire arm_resetn;
    wire pl_locked;
 
    mmcm_0 mmcm_0 (
      .clk_in1 ( GEN_200    ),
      .resetn  ( arm_resetn ),
      .clk_out1( pl_clk_50  ),
      .clk_out2( pl_clk_100 ),
      .clk_out3( pl_clk_200 ),
      .locked  ( pl_locked  ) 
    );

    rst_reg #(
      .INIT( 1'b1 )
    ) rst_reg_40 (
      .i_clk( arm_clk_40 ),
      .i_rst( ~pl_locked ),
      .o_rst( arm_rst_40 )
    );

    rst_reg #(
      .INIT( 1'b0 )
    ) rstn_reg_200 (
      .i_clk( pl_clk_200  ),
      .i_rst( pl_locked   ),
      .o_rst( pl_rstn_200 )
    );

  //---------------------------------------------------------------
  // Scan RX
  //---------------------------------------------------------------
    logic [16:0] scan_tdata;
    logic        scan_tvalid;
    logic        scan_tready;

    assign scan_tready = 1'b1;

    scan_core #(
      .LOG_NUM( 1 )
    ) scan_core_0 (
      .i_clk ( arm_clk_40 ),
      .i_srst( arm_rst_40 ), 
      
      .i_rx     ( SCAN        ),
      .i_en     ( '1          ),
      .o_tdata  ( scan_tdata  ),
      .o_tvalid ( scan_tvalid ),
      .i_tready ( scan_tready ),

      .i_lg_clk ( arm_clk_40 ),
      .i_lg_wren(  1'b1      ),
      .i_lg_num (  4'h3 ),
      .i_lg_addr( 16'h0 ),
      .i_lg_din ( 16'h0 ),
      .i_lg_wr  (  1'h0 ),
      .o_lg_dout( ),
      .o_lg_rdy ( )
    );
 
  //---------------------------------------------------------------
  // ARM Core
  //---------------------------------------------------------------
    arm_core arm_core_0(
      .io_PS_CLK      ( io_PS_CLK    ),
      .io_PS_PORB     ( io_PS_PORB   ),
      .io_PS_SRSTB    ( io_PS_SRSTB  ),
      .io_CPU_MIO     ( io_CPU_MIO   ),
      .io_DDR_CAS_n   ( io_DDR_CAS_n ),
      .io_DDR_CKE     ( io_DDR_CKE   ),
      .io_DDR_Clk_n   ( io_DDR_Clk_n ),
      .io_DDR_Clk     ( io_DDR_Clk   ),
      .io_DDR_CS_n    ( io_DDR_CS_n  ),
      .io_DDR_DRSTB   ( io_DDR_DRSTB ),
      .io_DDR_ODT     ( io_DDR_ODT   ),
      .io_DDR_RAS_n   ( io_DDR_RAS_n ),
      .io_DDR_WEB     ( io_DDR_WEB   ),
      .io_DDR_BankAddr( io_DDR_BA    ),
      .io_DDR_Addr    ( io_DDR_Addr  ),
      .io_DDR_VRN     ( io_DDR_VRN   ),
      .io_DDR_VRP     ( io_DDR_VRP   ),
      .io_DDR_DM      ( io_DDR_DM    ),
      .io_DDR_DQ      ( io_DDR_DQ    ),
      .io_DDR_DQS_n   ( io_DDR_DQS_n ),
      .io_DDR_DQS     ( io_DDR_DQS   ),

      .o_FCLK_CLK0    ( arm_clk_40   ),
      .o_FCLK_RESET0_N( arm_resetn   ),
      .i_clk_bus      ( pl_clk_200   ),
      .i_rstn_bus     ( pl_rstn_200  )
    );

endmodule
