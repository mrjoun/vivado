module axi3_master_ctrl #(
  parameter MAX_TRANS = 3
)(
  if_AXI3_4.M       m_bus_axi3,
  if_AXI4_Stream.S  s_bus_fifo_in,
  if_AXI4_Stream.M  m_bus_fifo_out,

  input [31:0] cmd_tdata,
  input [ 3:0] cmd_tdest,
  input        cmd_tvalid,
  output logic cmd_tready,

  output logic o_zynq_irq
);

  assign o_zynq_irq = '0;
//  assign m_bus_axi3.ARVALID = '0;
  assign m_bus_axi3.AWVALID = '0;
  assign m_bus_axi3.BREADY  = '0;
  assign m_bus_axi3.RREADY  = '1;
  assign m_bus_axi3.WLAST   = '0;
  assign m_bus_axi3.WVALID  = '0;
  assign m_bus_axi3.ARID    = '0;
  assign m_bus_axi3.ARPROT  = '0;
  assign m_bus_axi3.AWID    = '0;
  assign m_bus_axi3.AWPROT  = '0;
  assign m_bus_axi3.WID     = '0;
//  assign m_bus_axi3.ARADDR  = '0;
  assign m_bus_axi3.AWADDR  = '0;
  assign m_bus_axi3.ARCACHE = 'b0000;
//  assign m_bus_axi3.ARLEN   = '0;
  assign m_bus_axi3.ARQOS   = '0;
  assign m_bus_axi3.AWCACHE = 'b0000;
  assign m_bus_axi3.AWLEN   = '0;
  assign m_bus_axi3.AWQOS   = '0;
  assign m_bus_axi3.ARBURST = 'b01;
  assign m_bus_axi3.ARLOCK  = '0;
  assign m_bus_axi3.ARSIZE  = 'b011;
  assign m_bus_axi3.AWBURST = 'b01;
  assign m_bus_axi3.AWLOCK  = '0;
  assign m_bus_axi3.AWSIZE  = '0;
  assign m_bus_axi3.ARUSER  = 'b1;
  assign m_bus_axi3.AWUSER  = 'b1;
  assign m_bus_axi3.WDATA   = '0;
  assign m_bus_axi3.WSTRB   = '0;

  enum logic [2:0] {
    ST_IDLE  = 3'b001,
    ST_WRITE = 3'b010,
    ST_READ  = 3'b100
  } state_fsm;

  logic [31:0] min_addr_rd;
  logic [31:0] max_addr_rd;
  logic [31:0] ram_size;
  logic [31:0] min_addr_wr;
  logic [31:0] max_addr_wr;

  logic [m_bus_axi3.W_LN:0] prev_len;
  logic [ 3:0] active_read_trans;
  logic [31:0] read_size;
  logic reads_done;
  logic start_burst_read;
  logic burst_read_active;

  assign max_addr_wr = min_addr_wr + ram_size;
  assign max_addr_rd = min_addr_rd + ram_size;

  always @ (posedge m_bus_axi3.ACLK)
    if( m_bus_axi3.ARESETN == 1'b0 )
      min_addr_rd <= '0;
    else
      if( cmd_tready && cmd_tvalid && cmd_tdest[0] )
        min_addr_rd <= cmd_tdata;

  always @ (posedge m_bus_axi3.ACLK)
    if( m_bus_axi3.ARESETN == 1'b0 )
      m_bus_axi3.ARADDR <= '0;
    else
      if( m_bus_axi3.ARVALID && m_bus_axi3.ARREADY )
        m_bus_axi3.ARADDR[31:2] <= m_bus_axi3.ARADDR[31:2] + prev_len;

  always @ (posedge m_bus_axi3.ACLK)
    if( m_bus_axi3.ARESETN == 1'b0 )
      min_addr_wr <= '0;
    else
      if( cmd_tready && cmd_tvalid && cmd_tdest[1] )
        min_addr_wr <= cmd_tdata;

  always @ (posedge m_bus_axi3.ACLK)
    if( m_bus_axi3.ARESETN == 1'b0 )
      ram_size <= '0;
    else
      if( cmd_tready && cmd_tvalid && cmd_tdest[2] )
        ram_size <= cmd_tdata;

  always @ (posedge m_bus_axi3.ACLK)
    if( cmd_tready && cmd_tvalid && cmd_tdest[3] )
      read_size <= cmd_tdata;
    else
      if( m_bus_axi3.ARVALID && m_bus_axi3.ARREADY )
        read_size <= read_size - prev_len;

  always @ (posedge m_bus_axi3.ACLK)
    if( start_burst_read ) begin
      m_bus_axi3.ARLEN <= (read_size > 16) ? '1 : read_size-1;
      prev_len         <= (read_size > 16) ? 16 : read_size;
    end

  always @ (posedge m_bus_axi3.ACLK)
    if( cmd_tready && cmd_tvalid ) 
      active_read_trans <= '0;
    else
      if( m_bus_axi3.ARVALID && m_bus_axi3.ARREADY )
        active_read_trans <= active_read_trans + 1;
      else
        if( m_bus_axi3.RVALID && m_bus_axi3.RLAST )
          active_read_trans <= active_read_trans - 1;

  always @ (posedge m_bus_axi3.ACLK)
    if( m_bus_axi3.ARESETN == 1'b0 ) begin
      state_fsm        <= ST_IDLE;
      cmd_tready       <= 1'b0;
      start_burst_read <= 1'b0;
    end
    else
      case( state_fsm ) 
        ST_IDLE: begin
          cmd_tready <= 1'b1;
          if( cmd_tready && cmd_tvalid && cmd_tdest[3] ) begin
            cmd_tready <= 1'b0;
            state_fsm  <= ST_READ;
          end
          else
            state_fsm <= ST_IDLE;
        end
        ST_READ: begin
          state_fsm        <= ST_READ;
          start_burst_read <= ~m_bus_axi3.ARVALID && ~start_burst_read && |read_size & (active_read_trans != MAX_TRANS);
        end
      endcase

  always @ (posedge m_bus_axi3.ACLK)
    if( m_bus_axi3.ARESETN == 1'b0 )
      m_bus_axi3.ARVALID <= 1'b0;
    else
      if( ~m_bus_axi3.ARVALID && start_burst_read )
        m_bus_axi3.ARVALID <= 1'b1;
      else
        if( m_bus_axi3.ARREADY && m_bus_axi3.ARVALID )
          m_bus_axi3.ARVALID <= 1'b0;

 endmodule
