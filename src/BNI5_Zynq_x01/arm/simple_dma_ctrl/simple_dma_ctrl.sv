module simple_dma_ctrl(
  if_AXI3_4.M                  m_bus_axi3,
  if_AXI3_4.S                  s_bus_axi4,

  output [m_bus_axi3.W_DT-1:0] o_tdata,
  output                       o_tvalid,
  input                        i_tready,

  input  [m_bus_axi3.W_DT-1:0] i_tdata,
  input                        i_tvalid,
  output                       o_tready,

  output logic                 o_irq
);
/*
  1) i_tvalid == 1;
  2) o_tready <= 1;
  3) if( (counter == size/2) || (16 tick (i_tvalid == 0)) ) o_irq = 1;
  4) wait( o_irq == 0 )
  5) goto 1
  
  250  * 16 = 125   * 32 = 62.5*64
  62.5 * 16 = 31.25 * 32 = 15.5*64
  
  [0] - RD/WR - RAM_ADDR_RD
  [1] - RD/WR - RAM_ADDR_WR
  [2] -    WR - RAM_SIZE
  [3] - RD/WR - RUN_READ_SIZE
  [4] -    WR - CLEAR_IRQ
*/
  //--------------------------------------------------------------------------------------------
  // AXI4 slave ( without BURST )
  //--------------------------------------------------------------------------------------------
    logic [31:0] axi4_slv_tdata;
    logic [ 3:0] axi4_slv_tdest;
    logic        axi4_slv_tvalid;
    logic        axi4_slv_tready;
    logic        clear_irq;
   
    axi4_slave_ctrl axi4_slave_ctrl_0(
      .s_bus_axi4   ( s_bus_axi4      ),
      .o_fifo_tdata ( axi4_slv_tdata  ),
      .o_fifo_tdest ( axi4_slv_tdest  ),
      .o_fifo_tvalid( axi4_slv_tvalid ),
      .i_fifo_tready( axi4_slv_tready ),
      .o_clear_irq  ( clear_irq       )
    );
 
  //--------------------------------------------------------------------------------------------
  // FIFO COMMAND AXI4Stream
  //--------------------------------------------------------------------------------------------
    logic [31:0] axi3_mst_tdata;
    logic [ 3:0] axi3_mst_tdest;
    logic        axi3_mst_tvalid;
    logic        axi3_mst_tready;
    
    fifos_16x36_fwfs fifo_command_0(
      .s_aclk       ( s_bus_axi4.ACLK    ),
      .s_aresetn    ( s_bus_axi4.ARESETN ),
      .s_axis_tvalid( axi4_slv_tvalid    ),
      .s_axis_tready( axi4_slv_tready    ),
      .s_axis_tdata ( axi4_slv_tdata     ),
      .s_axis_tdest ( axi4_slv_tdest     ),

      .m_aclk       ( m_bus_axi3.ACLK    ),
      .m_axis_tvalid( axi3_mst_tvalid    ),
      .m_axis_tready( axi3_mst_tready    ),
      .m_axis_tdata ( axi3_mst_tdata     ),
      .m_axis_tdest ( axi3_mst_tdest     )
    );
  
  //--------------------------------------------------------------------------------------------
  // AXI3 master
  //--------------------------------------------------------------------------------------------
    if_AXI4_Stream #( m_bus_axi3.W_DT ) stream_in_zynq  ();
    if_AXI4_Stream #( m_bus_axi3.W_DT ) stream_out_zynq (); 
    
    assign stream_in_zynq.TDATA  = i_tdata;
    assign stream_in_zynq.TVALID = i_tvalid;
    assign o_tready = stream_in_zynq.TREADY;

    assign o_tdata  = stream_out_zynq.TDATA;
    assign o_tvalid = stream_out_zynq.TVALID;
    assign stream_out_zynq.TREADY = i_tready;

    axi3_master_ctrl axi3_master_ctrl_0(
      .m_bus_axi3    ( m_bus_axi3      ),
      .s_bus_fifo_in ( stream_in_zynq  ),
      .m_bus_fifo_out( stream_out_zynq ),

      .cmd_tdata ( axi3_mst_tdata  ),
      .cmd_tdest ( axi3_mst_tdest  ),
      .cmd_tvalid( axi3_mst_tvalid ),
      .cmd_tready( axi3_mst_tready ), 

      .o_zynq_irq( o_irq )
    );

endmodule
