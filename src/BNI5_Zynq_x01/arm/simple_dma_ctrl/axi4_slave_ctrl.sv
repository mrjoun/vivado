module axi4_slave_ctrl (
  if_AXI3_4.S s_bus_axi4,

  output reg [31:0] o_fifo_tdata,
  output reg [ 3:0] o_fifo_tdest,
  output reg        o_fifo_tvalid,
  input             i_fifo_tready,

  output reg        o_clear_irq
);

  reg [4:0] o_addr;

  assign s_bus_axi4.RLAST = 1'b1;
  assign s_bus_axi4.BRESP = 2'b00;
  assign s_bus_axi4.RRESP = 2'b00;
  assign s_bus_axi4.RDATA = '0;

  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.ARESETN == 1'b0 ) begin
      o_clear_irq        <= 1'b0;
      s_bus_axi4.AWREADY <= 1'b0;
      s_bus_axi4.ARREADY <= 1'b0;
      s_bus_axi4.WREADY  <= 1'b0;
    end
    else begin
      o_clear_irq        <=  s_bus_axi4.WREADY  && s_bus_axi4.WVALID && o_addr[4]; 
      s_bus_axi4.ARREADY <= ~s_bus_axi4.ARREADY && s_bus_axi4.ARVALID;
      s_bus_axi4.AWREADY <= ~s_bus_axi4.AWREADY && s_bus_axi4.AWVALID;
      s_bus_axi4.WREADY  <= ~s_bus_axi4.WREADY  && s_bus_axi4.WVALID && i_fifo_tready && o_fifo_tvalid;
    end

  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.ARESETN == 1'b0 ) 
      o_fifo_tvalid <= 1'b0;
    else
      if( s_bus_axi4.WVALID && ~o_fifo_tvalid && ~s_bus_axi4.WREADY )
        o_fifo_tvalid <= 1'b1;
      else
        if( i_fifo_tready ) 
          o_fifo_tvalid <= 1'b0;

  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.WVALID ) begin
      o_fifo_tdata <= s_bus_axi4.WDATA;
      o_fifo_tdest <= o_addr[3:0];
    end

  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.AWVALID )
      o_addr <= s_bus_axi4.AWADDR[6:2];
    else
      if( s_bus_axi4.ARVALID )
        o_addr <= s_bus_axi4.ARADDR[6:2];

  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.AWREADY )
      s_bus_axi4.BID <= s_bus_axi4.AWID;

  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.ARREADY )
      s_bus_axi4.RID <= s_bus_axi4.ARID;
  
  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.ARESETN == 1'b0 )
      s_bus_axi4.BVALID <= 1'b0;
    else
      if( s_bus_axi4.WREADY && s_bus_axi4.WVALID  && ~s_bus_axi4.BVALID )
        s_bus_axi4.BVALID <= 1'h1;
      else
        if( s_bus_axi4.BVALID && s_bus_axi4.BREADY )
          s_bus_axi4.BVALID <= 1'b0;
    
  always @ (posedge s_bus_axi4.ACLK)
    if( s_bus_axi4.ARESETN == 1'b0 ) 
      s_bus_axi4.RVALID <= 1'b0;
    else
      if( s_bus_axi4.ARREADY && s_bus_axi4.ARVALID && ~s_bus_axi4.RVALID )
        s_bus_axi4.RVALID <= 1'h1;
      else
        if( s_bus_axi4.RVALID && s_bus_axi4.RREADY ) 
          s_bus_axi4.RVALID <= 1'b0;  

endmodule
