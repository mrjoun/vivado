`include "interfaces_arm.svh"

module arm_core(
  //ARM CPU PINS
  inout [53:0] io_CPU_MIO,
  inout        io_DDR_CAS_n,
  inout        io_DDR_CKE,
  inout        io_DDR_Clk_n,
  inout        io_DDR_Clk,
  inout        io_DDR_CS_n,
  inout        io_DDR_DRSTB,
  inout        io_DDR_ODT,
  inout        io_DDR_RAS_n,
  inout        io_DDR_WEB,
  inout [ 2:0] io_DDR_BankAddr,
  inout [14:0] io_DDR_Addr,
  inout        io_DDR_VRN,
  inout        io_DDR_VRP,
  inout [ 3:0] io_DDR_DM,
  inout [31:0] io_DDR_DQ,
  inout [ 3:0] io_DDR_DQS_n,
  inout [ 3:0] io_DDR_DQS,
  inout        io_PS_SRSTB,
  inout        io_PS_CLK,
  inout        io_PS_PORB,

  output       o_FCLK_CLK0,
  output       o_FCLK_RESET0_N,
  input        i_clk_bus,
  input        i_rstn_bus
);

  //---------------------------------------------------------
  // Bus interface
  //---------------------------------------------------------
    if_AXI3_4 #( 
      .W_ID( 3),
      .W_DT(64),
      .W_AD(32),
      .W_LN( 4),
      .W_BR( 2),
      .W_US( 5)
    ) bus_axi3_acp ( 
      i_clk_bus, 
      i_rstn_bus
    );
  
    if_AXI3_4 #( 
      .W_ID( 6),
      .W_DT(64),
      .W_AD(32),
      .W_LN( 4),
      .W_BR( 2),
      .W_US( 5)
    ) bus_axi3_hp0 ( 
      i_clk_bus,
      i_rstn_bus
    );

    if_AXI3_4 #( 
      .W_ID(12),
      .W_DT(32),
      .W_AD(32),
      .W_LN( 4),
      .W_BR( 2),
      .W_US( 1)
    ) bus_axi4_gp0 ( 
      i_clk_bus,
      i_rstn_bus
    );
  
    if_AXI3_4 #( 
      .W_ID(12),
      .W_DT(32),
      .W_AD(32),
      .W_LN( 4),
      .W_BR( 2),
      .W_US( 1)
    ) bus_axi4_gp1 ( 
      i_clk_bus,
      i_rstn_bus
    );
  
  //---------------------------------------------------------
    wire        irq0, irq1;
    wire [15:0] irq;

    assign irq = {14'h0, irq1, irq0};
  
  
  processing_system7_0 arm_inst (
    .IRQ_F2P      ( irq             ),
    .FCLK_CLK0    ( o_FCLK_CLK0     ),
    .FCLK_RESET0_N( o_FCLK_RESET0_N ),
    .MIO          ( io_CPU_MIO      ),
    .PS_CLK       ( io_PS_CLK       ),
    .PS_PORB      ( io_PS_PORB      ),
    .PS_SRSTB     ( io_PS_SRSTB     ),
    //DDR ports
    .DDR_CAS_n    ( io_DDR_CAS_n    ),
    .DDR_CKE      ( io_DDR_CKE      ),
    .DDR_Clk_n    ( io_DDR_Clk_n    ),
    .DDR_Clk      ( io_DDR_Clk      ),
    .DDR_CS_n     ( io_DDR_CS_n     ),
    .DDR_DRSTB    ( io_DDR_DRSTB    ),
    .DDR_ODT      ( io_DDR_ODT      ),
    .DDR_RAS_n    ( io_DDR_RAS_n    ),
    .DDR_WEB      ( io_DDR_WEB      ),
    .DDR_BankAddr ( io_DDR_BankAddr ),
    .DDR_Addr     ( io_DDR_Addr     ),
    .DDR_VRN      ( io_DDR_VRN      ),
    .DDR_VRP      ( io_DDR_VRP      ),
    .DDR_DM       ( io_DDR_DM       ),
    .DDR_DQ       ( io_DDR_DQ       ),
    .DDR_DQS_n    ( io_DDR_DQS_n    ),
    .DDR_DQS      ( io_DDR_DQS      ),
    //GP0 ports
    .M_AXI_GP0_ACLK   ( bus_axi4_gp0.ACLK    ),
    .M_AXI_GP0_ARVALID( bus_axi4_gp0.ARVALID ),
    .M_AXI_GP0_AWVALID( bus_axi4_gp0.AWVALID ),
    .M_AXI_GP0_BREADY ( bus_axi4_gp0.BREADY  ),
    .M_AXI_GP0_RREADY ( bus_axi4_gp0.RREADY  ),
    .M_AXI_GP0_WLAST  ( bus_axi4_gp0.WLAST   ),
    .M_AXI_GP0_WVALID ( bus_axi4_gp0.WVALID  ),
    .M_AXI_GP0_ARID   ( bus_axi4_gp0.ARID    ),
    .M_AXI_GP0_AWID   ( bus_axi4_gp0.AWID    ),
    .M_AXI_GP0_WID    ( bus_axi4_gp0.WID     ),
    .M_AXI_GP0_ARBURST( bus_axi4_gp0.ARBURST ),
    .M_AXI_GP0_ARLOCK ( bus_axi4_gp0.ARLOCK  ),
    .M_AXI_GP0_ARSIZE ( bus_axi4_gp0.ARSIZE  ),
    .M_AXI_GP0_AWBURST( bus_axi4_gp0.AWBURST ),
    .M_AXI_GP0_AWLOCK ( bus_axi4_gp0.AWLOCK  ),
    .M_AXI_GP0_AWSIZE ( bus_axi4_gp0.AWSIZE  ),
    .M_AXI_GP0_ARPROT ( bus_axi4_gp0.ARPROT  ),
    .M_AXI_GP0_AWPROT ( bus_axi4_gp0.AWPROT  ),
    .M_AXI_GP0_ARADDR ( bus_axi4_gp0.ARADDR  ),
    .M_AXI_GP0_AWADDR ( bus_axi4_gp0.AWADDR  ),
    .M_AXI_GP0_WDATA  ( bus_axi4_gp0.WDATA   ),
    .M_AXI_GP0_ARCACHE( bus_axi4_gp0.ARCACHE ),
    .M_AXI_GP0_ARLEN  ( bus_axi4_gp0.ARLEN   ),
    .M_AXI_GP0_ARQOS  ( bus_axi4_gp0.ARQOS   ),
    .M_AXI_GP0_AWCACHE( bus_axi4_gp0.AWCACHE ),
    .M_AXI_GP0_AWLEN  ( bus_axi4_gp0.AWLEN   ),
    .M_AXI_GP0_AWQOS  ( bus_axi4_gp0.AWQOS   ),
    .M_AXI_GP0_WSTRB  ( bus_axi4_gp0.WSTRB   ),
    .M_AXI_GP0_ARREADY( bus_axi4_gp0.ARREADY ),
    .M_AXI_GP0_AWREADY( bus_axi4_gp0.AWREADY ),
    .M_AXI_GP0_BVALID ( bus_axi4_gp0.BVALID  ),
    .M_AXI_GP0_RLAST  ( bus_axi4_gp0.RLAST   ),
    .M_AXI_GP0_RVALID ( bus_axi4_gp0.RVALID  ),
    .M_AXI_GP0_WREADY ( bus_axi4_gp0.WREADY  ),
    .M_AXI_GP0_BID    ( bus_axi4_gp0.BID     ),
    .M_AXI_GP0_RID    ( bus_axi4_gp0.RID     ),
    .M_AXI_GP0_BRESP  ( bus_axi4_gp0.BRESP   ),
    .M_AXI_GP0_RRESP  ( bus_axi4_gp0.RRESP   ),
    .M_AXI_GP0_RDATA  ( bus_axi4_gp0.RDATA   ),
    //GP1 ports
    .M_AXI_GP1_ACLK   ( bus_axi4_gp1.ACLK    ),
    .M_AXI_GP1_ARVALID( bus_axi4_gp1.ARVALID ),
    .M_AXI_GP1_AWVALID( bus_axi4_gp1.AWVALID ),
    .M_AXI_GP1_BREADY ( bus_axi4_gp1.BREADY  ),
    .M_AXI_GP1_RREADY ( bus_axi4_gp1.RREADY  ),
    .M_AXI_GP1_WLAST  ( bus_axi4_gp1.WLAST   ),
    .M_AXI_GP1_WVALID ( bus_axi4_gp1.WVALID  ),
    .M_AXI_GP1_ARID   ( bus_axi4_gp1.ARID    ),
    .M_AXI_GP1_AWID   ( bus_axi4_gp1.AWID    ),
    .M_AXI_GP1_WID    ( bus_axi4_gp1.WID     ),
    .M_AXI_GP1_ARBURST( bus_axi4_gp1.ARBURST ),
    .M_AXI_GP1_ARLOCK ( bus_axi4_gp1.ARLOCK  ),
    .M_AXI_GP1_ARSIZE ( bus_axi4_gp1.ARSIZE  ),
    .M_AXI_GP1_AWBURST( bus_axi4_gp1.AWBURST ),
    .M_AXI_GP1_AWLOCK ( bus_axi4_gp1.AWLOCK  ),
    .M_AXI_GP1_AWSIZE ( bus_axi4_gp1.AWSIZE  ),
    .M_AXI_GP1_ARPROT ( bus_axi4_gp1.ARPROT  ),
    .M_AXI_GP1_AWPROT ( bus_axi4_gp1.AWPROT  ),
    .M_AXI_GP1_ARADDR ( bus_axi4_gp1.ARADDR  ),
    .M_AXI_GP1_AWADDR ( bus_axi4_gp1.AWADDR  ),
    .M_AXI_GP1_WDATA  ( bus_axi4_gp1.WDATA   ),
    .M_AXI_GP1_ARCACHE( bus_axi4_gp1.ARCACHE ),
    .M_AXI_GP1_ARLEN  ( bus_axi4_gp1.ARLEN   ),
    .M_AXI_GP1_ARQOS  ( bus_axi4_gp1.ARQOS   ),
    .M_AXI_GP1_AWCACHE( bus_axi4_gp1.AWCACHE ),
    .M_AXI_GP1_AWLEN  ( bus_axi4_gp1.AWLEN   ),
    .M_AXI_GP1_AWQOS  ( bus_axi4_gp1.AWQOS   ),
    .M_AXI_GP1_WSTRB  ( bus_axi4_gp1.WSTRB   ),
    .M_AXI_GP1_ARREADY( bus_axi4_gp1.ARREADY ),
    .M_AXI_GP1_AWREADY( bus_axi4_gp1.AWREADY ),
    .M_AXI_GP1_BVALID ( bus_axi4_gp1.BVALID  ),
    .M_AXI_GP1_RLAST  ( bus_axi4_gp1.RLAST   ),
    .M_AXI_GP1_RVALID ( bus_axi4_gp1.RVALID  ),
    .M_AXI_GP1_WREADY ( bus_axi4_gp1.WREADY  ),
    .M_AXI_GP1_BID    ( bus_axi4_gp1.BID     ),
    .M_AXI_GP1_RID    ( bus_axi4_gp1.RID     ),
    .M_AXI_GP1_BRESP  ( bus_axi4_gp1.BRESP   ),
    .M_AXI_GP1_RRESP  ( bus_axi4_gp1.RRESP   ),
    .M_AXI_GP1_RDATA  ( bus_axi4_gp1.RDATA   ),
    //HP0 ports
    .S_AXI_HP0_ACLK   ( bus_axi3_hp0.ACLK    ),
    .S_AXI_HP0_ARREADY( bus_axi3_hp0.ARREADY ),
    .S_AXI_HP0_AWREADY( bus_axi3_hp0.AWREADY ),
    .S_AXI_HP0_BVALID ( bus_axi3_hp0.BVALID  ),
    .S_AXI_HP0_RLAST  ( bus_axi3_hp0.RLAST   ),
    .S_AXI_HP0_RVALID ( bus_axi3_hp0.RVALID  ),
    .S_AXI_HP0_WREADY ( bus_axi3_hp0.WREADY  ),
    .S_AXI_HP0_BRESP  ( bus_axi3_hp0.BRESP   ),
    .S_AXI_HP0_RRESP  ( bus_axi3_hp0.RRESP   ),
    .S_AXI_HP0_BID    ( bus_axi3_hp0.BID     ),
    .S_AXI_HP0_RID    ( bus_axi3_hp0.RID     ),
    .S_AXI_HP0_RDATA  ( bus_axi3_hp0.RDATA   ),
    .S_AXI_HP0_ARVALID( bus_axi3_hp0.ARVALID ),
    .S_AXI_HP0_AWVALID( bus_axi3_hp0.AWVALID ),
    .S_AXI_HP0_BREADY ( bus_axi3_hp0.BREADY  ),
    .S_AXI_HP0_RREADY ( bus_axi3_hp0.RREADY  ),
    .S_AXI_HP0_WLAST  ( bus_axi3_hp0.WLAST   ),
    .S_AXI_HP0_WVALID ( bus_axi3_hp0.WVALID  ),
    .S_AXI_HP0_ARBURST( bus_axi3_hp0.ARBURST ),
    .S_AXI_HP0_ARLOCK ( bus_axi3_hp0.ARLOCK  ),
    .S_AXI_HP0_ARSIZE ( bus_axi3_hp0.ARSIZE  ),
    .S_AXI_HP0_AWBURST( bus_axi3_hp0.AWBURST ),
    .S_AXI_HP0_AWLOCK ( bus_axi3_hp0.AWLOCK  ),
    .S_AXI_HP0_AWSIZE ( bus_axi3_hp0.AWSIZE  ),
    .S_AXI_HP0_ARPROT ( bus_axi3_hp0.ARPROT  ),
    .S_AXI_HP0_AWPROT ( bus_axi3_hp0.AWPROT  ),
    .S_AXI_HP0_ARADDR ( bus_axi3_hp0.ARADDR  ),
    .S_AXI_HP0_AWADDR ( bus_axi3_hp0.AWADDR  ),
    .S_AXI_HP0_ARCACHE( bus_axi3_hp0.ARCACHE ),
    .S_AXI_HP0_ARLEN  ( bus_axi3_hp0.ARLEN   ),
    .S_AXI_HP0_ARQOS  ( bus_axi3_hp0.ARQOS   ),
    .S_AXI_HP0_AWCACHE( bus_axi3_hp0.AWCACHE ),
    .S_AXI_HP0_AWLEN  ( bus_axi3_hp0.AWLEN   ),
    .S_AXI_HP0_AWQOS  ( bus_axi3_hp0.AWQOS   ),
    .S_AXI_HP0_ARID   ( bus_axi3_hp0.ARID    ),
    .S_AXI_HP0_AWID   ( bus_axi3_hp0.AWID    ),
    .S_AXI_HP0_WID    ( bus_axi3_hp0.WID     ),
    .S_AXI_HP0_WDATA  ( bus_axi3_hp0.WDATA   ),
    .S_AXI_HP0_WSTRB  ( bus_axi3_hp0.WSTRB   ),
    //HP0_FIFO_CTRL
    .S_AXI_HP0_RCOUNT ( ),
    .S_AXI_HP0_WCOUNT ( ),
    .S_AXI_HP0_RACOUNT( ),
    .S_AXI_HP0_WACOUNT( ),
    .S_AXI_HP0_RDISSUECAP1_EN( 1'b0 ),
    .S_AXI_HP0_WRISSUECAP1_EN( 1'b0 ),
    //ACP ports
    .S_AXI_ACP_ACLK   ( bus_axi3_acp.ACLK    ),
    .S_AXI_ACP_ARREADY( bus_axi3_acp.ARREADY ),
    .S_AXI_ACP_AWREADY( bus_axi3_acp.AWREADY ),
    .S_AXI_ACP_BVALID ( bus_axi3_acp.BVALID  ),
    .S_AXI_ACP_RLAST  ( bus_axi3_acp.RLAST   ),
    .S_AXI_ACP_RVALID ( bus_axi3_acp.RVALID  ),
    .S_AXI_ACP_WREADY ( bus_axi3_acp.WREADY  ),
    .S_AXI_ACP_BRESP  ( bus_axi3_acp.BRESP   ),
    .S_AXI_ACP_RRESP  ( bus_axi3_acp.RRESP   ),
    .S_AXI_ACP_BID    ( bus_axi3_acp.BID     ),
    .S_AXI_ACP_RID    ( bus_axi3_acp.RID     ),
    .S_AXI_ACP_RDATA  ( bus_axi3_acp.RDATA   ),
    .S_AXI_ACP_ARVALID( bus_axi3_acp.ARVALID ),
    .S_AXI_ACP_AWVALID( bus_axi3_acp.AWVALID ),
    .S_AXI_ACP_BREADY ( bus_axi3_acp.BREADY  ),
    .S_AXI_ACP_RREADY ( bus_axi3_acp.RREADY  ),
    .S_AXI_ACP_WLAST  ( bus_axi3_acp.WLAST   ),
    .S_AXI_ACP_WVALID ( bus_axi3_acp.WVALID  ),
    .S_AXI_ACP_ARID   ( bus_axi3_acp.ARID    ),
    .S_AXI_ACP_ARPROT ( bus_axi3_acp.ARPROT  ),
    .S_AXI_ACP_AWID   ( bus_axi3_acp.AWID    ),
    .S_AXI_ACP_AWPROT ( bus_axi3_acp.AWPROT  ),
    .S_AXI_ACP_WID    ( bus_axi3_acp.WID     ),
    .S_AXI_ACP_ARADDR ( bus_axi3_acp.ARADDR  ),
    .S_AXI_ACP_AWADDR ( bus_axi3_acp.AWADDR  ),
    .S_AXI_ACP_ARCACHE( bus_axi3_acp.ARCACHE ),
    .S_AXI_ACP_ARLEN  ( bus_axi3_acp.ARLEN   ),
    .S_AXI_ACP_ARQOS  ( bus_axi3_acp.ARQOS   ),
    .S_AXI_ACP_AWCACHE( bus_axi3_acp.AWCACHE ),
    .S_AXI_ACP_AWLEN  ( bus_axi3_acp.AWLEN   ),
    .S_AXI_ACP_AWQOS  ( bus_axi3_acp.AWQOS   ),
    .S_AXI_ACP_ARBURST( bus_axi3_acp.ARBURST ),
    .S_AXI_ACP_ARLOCK ( bus_axi3_acp.ARLOCK  ),
    .S_AXI_ACP_ARSIZE ( bus_axi3_acp.ARSIZE  ),
    .S_AXI_ACP_AWBURST( bus_axi3_acp.AWBURST ),
    .S_AXI_ACP_AWLOCK ( bus_axi3_acp.AWLOCK  ),
    .S_AXI_ACP_AWSIZE ( bus_axi3_acp.AWSIZE  ),
    .S_AXI_ACP_ARUSER ( bus_axi3_acp.ARUSER  ),
    .S_AXI_ACP_AWUSER ( bus_axi3_acp.AWUSER  ),
    .S_AXI_ACP_WDATA  ( bus_axi3_acp.WDATA   ),
    .S_AXI_ACP_WSTRB  ( bus_axi3_acp.WSTRB   )
  );

  simple_dma_ctrl tcp_data_ctrl_0 (
    .m_bus_axi3( bus_axi3_hp0.M ),
    .s_bus_axi4( bus_axi4_gp0.S ),

    .o_tdata ( ),
    .o_tvalid( ),
    .i_tready( '1 ),
    
    .i_tdata ( '0 ),
    .i_tvalid( '0 ),
    .o_tready( ),

    .o_irq   ( irq0 )
  );

  simple_dma_ctrl tcp_cmd_ctrl_0 (
    .m_bus_axi3( bus_axi3_acp.M ),
    .s_bus_axi4( bus_axi4_gp1.S ),

    .o_tdata ( ),
    .o_tvalid( ),
    .i_tready( '1 ),

    .i_tdata ( '0 ),
    .i_tvalid( '0 ),
    .o_tready( ),

    .o_irq   ( irq1 )
  );

endmodule
