`ifndef __INTERFACE_ARM
`define __INTERFACE_ARM
  
  interface if_AXI4_Stream #( parameter WD = 32 );
    logic [WD-1:0] TDATA;
    logic          TVALID;
    logic          TREADY;
    
    modport S (
      input  TDATA,
      input  TVALID,
      output TREADY
    );

    modport M (
      output TDATA,
      output TVALID,
      input  TREADY
    );

  endinterface

  interface if_AXI3_4
   #( parameter W_ID = 2,  parameter W_DT = 64, 
      parameter W_AD = 32, parameter W_LN = 4, 
      parameter W_BR = 2,  parameter W_US = 5)
    ( input bit ACLK, input bit ARESETN );
    //Read address channel signals
    logic [W_AD-1:0] ARADDR;
    logic            ARVALID;
    logic            ARREADY;
    logic [W_ID-1:0] ARID;
    logic [     2:0] ARPROT;
    logic [     3:0] ARCACHE;
    logic [W_LN-1:0] ARLEN;
    logic [     3:0] ARQOS;
    logic [W_BR-1:0] ARBURST;
    logic [     1:0] ARLOCK;
    logic [     2:0] ARSIZE;
    logic [W_US-1:0] ARUSER;
    //Write address channel signals
    logic [W_AD-1:0] AWADDR;
    logic            AWVALID;
    logic            AWREADY;
    logic [W_ID-1:0] AWID;
    logic [     2:0] AWPROT;
    logic [     3:0] AWCACHE;
    logic [W_LN-1:0] AWLEN;
    logic [     3:0] AWQOS;
    logic [W_BR-1:0] AWBURST;
    logic [     1:0] AWLOCK;
    logic [     2:0] AWSIZE;
    logic [W_US-1:0] AWUSER;
    //Write response channel signals
    logic            BVALID;
    logic            BREADY;
    logic [     1:0] BRESP;
    logic [W_ID-1:0] BID;
    //Read data channel signals
    logic            RVALID;
    logic            RREADY;
    logic            RLAST;
    logic [W_DT-1:0] RDATA;
    logic [W_ID-1:0] RID;
    logic [     1:0] RRESP;
    //Write data channel signals
    logic            WVALID;
    logic            WREADY;
    logic            WLAST;
    logic [W_DT-1:0] WDATA;
    logic [W_DT/8-1:0] WSTRB;
    logic [W_ID-1:0] WID;

    modport S (
      input  ACLK,
      input  ARESETN,
      output ARREADY,
      output AWREADY,
      output BVALID,
      output RLAST,
      output RVALID,
      output WREADY,
      output BRESP,
      output RRESP,
      output BID,
      output RID,
      output RDATA,
      input  ARVALID,
      input  AWVALID,
      input  BREADY,
      input  RREADY,
      input  WLAST,
      input  WVALID,
      input  ARID,
      input  ARPROT,
      input  AWID,
      input  AWPROT,
      input  WID,
      input  ARADDR,
      input  AWADDR,
      input  ARCACHE,
      input  ARLEN,
      input  ARQOS,
      input  AWCACHE,
      input  AWLEN,
      input  AWQOS,
      input  ARBURST,
      input  ARLOCK,
      input  ARSIZE,
      input  AWBURST,
      input  AWLOCK,
      input  AWSIZE,
      input  ARUSER,
      input  AWUSER,
      input  WDATA,
      input  WSTRB
    );
 
    modport M (
      input  ACLK,
      input  ARESETN,
      input  ARREADY,
      input  AWREADY,
      input  BVALID,
      input  RLAST,
      input  RVALID,
      input  WREADY,
      input  BRESP,
      input  RRESP,
      input  BID,
      input  RID,
      input  RDATA,
      output ARVALID,
      output AWVALID,
      output BREADY,
      output RREADY,
      output WLAST,
      output WVALID,
      output ARID,
      output ARPROT,
      output AWID,
      output AWPROT,
      output WID,
      output ARADDR,
      output AWADDR,
      output ARCACHE,
      output ARLEN,
      output ARQOS,
      output AWCACHE,
      output AWLEN,
      output AWQOS,
      output ARBURST,
      output ARLOCK,
      output ARSIZE,
      output AWBURST,
      output AWLOCK,
      output AWSIZE,
      output ARUSER,
      output AWUSER,
      output WDATA,
      output WSTRB
    );

  endinterface

`endif
