#---------------------------------------------------------------------------
# Function
#---------------------------------------------------------------------------
  proc read_settings_proj { path_project } {
    if [catch {set fd [open "$path_project/info.settings" r] } errmsg] {
      puts " > Cannot find file settings at \"$path_project/info.settings\""
      return [list 0]
    } else {
      set file_data [read $fd]
      close $fd
      set data [split $file_data "\n"]
      set res {}
      foreach line $data {
        set pos   [string last : $line]
        set name  [string range $line 0 $pos-1]
        set value [string range $line $pos+1 end]
        lappend res $value
      }
      return $res
    }
  }

  proc create_dir { path } {
    if { [file exists $path ] == 1 } {
      file delete -force $path
    }
    file mkdir $path
  }

  proc findFiles { basedir pattern filter } {
    set basedir [string trimright [file join [file normalize $basedir] { }]]
    set fileList {}
    set len_filter [llength $filter]

    foreach fileName [glob -nocomplain -type {f r} -path $basedir $pattern] {
      set not_find 0
      if { [llength $filter] == 0 } {
        lappend fileList $fileName
      } else {
        foreach filt_n $filter {
          #puts "filter: $filt_n"
          if { ![string match $filt_n $fileName] } {
           # lappend fileList $fileName
            incr not_find
            #puts "not find"
          } else {
            #set not_find $len_filter
            break
          }
        }
        if { $not_find == $len_filter } {
          #puts "Not find in filter"
          lappend fileList $fileName 
        }
      }
    }
    foreach dirName [glob -nocomplain -type {d  r} -path $basedir *] {
      set subDirList [findFiles $dirName $pattern $filter]
      if { [llength $subDirList] > 0 } {
        foreach subDirFile $subDirList {
          lappend fileList $subDirFile
        }
      }
    }
    return $fileList
  }

#---------------------------------------------------------------------------

set vivNm "vivado"
set compDir "/opt/compilelibs_vivado"
set tclPath [file dirname [file normalize [info script]]]

create_dir "$tclPath/$vivNm"

foreach prj_dir [glob -type d $tclPath/src/*] {
  set topName [string range $prj_dir [string last / $prj_dir]+1 end] 
  set answer [read_settings_proj $prj_dir]
  if { [llength $answer] == 5 } {
    set partDev    [lindex $answer 0]
    set syn_top_nm [lindex $answer 1]
    set sim_top_nm [lindex $answer 2]
    set dir_ip     [lindex $answer 3]
    set dir_tb     [lindex $answer 4]
    set srcVHD     [findFiles $prj_dir "*.vhd" [list "$prj_dir/$dir_ip/*" "$prj_dir/$dir_tb/*" ] ]
    set srcVer     [findFiles $prj_dir "*.v"   [list "$prj_dir/$dir_ip/*" "$prj_dir/$dir_tb/*" ] ]
    set srcXCI     [findFiles $prj_dir "*.xci" [list ""] ]
    set srcXDC     [findFiles $prj_dir "*.xdc" [list "$prj_dir/$dir_ip/*" "$prj_dir/$dir_tb/*" ] ]
    set srcSV      [findFiles $prj_dir "*.sv"  [list "$prj_dir/$dir_ip/*" "$prj_dir/$dir_tb/*" ] ]
    set srcSVH     [findFiles $prj_dir "*.svh" [list "$prj_dir/$dir_ip/*" "$prj_dir/$dir_tb/*" ] ]
    set srcTB      [findFiles "$prj_dir/testbench" "*.*v*" [list ""] ];
    
    create_dir "$tclPath/$vivNm/$topName/"
    cd "$tclPath/$vivNm/$topName/"

    create_project -force $topName "$tclPath/$vivNm/$topName/" -part $partDev
    set_property target_language VERILOG [current_project]

    if { [llength $srcXCI] > 0 } {
      add_files -norecurse $srcXCI
    }
    export_ip_user_files -of_objects [get_files $srcXCI] -force -quiet

    if { [llength $srcVHD] > 0 } {
      add_files $srcVHD
    }
    if { [llength $srcVer] > 0 } {
      add_files $srcVer
    }
    if { [llength $srcSV] > 0 } {
      add_files $srcSV
    }
    if { [llength $srcSVH] > 0 } {
      add_files $srcSVH
    }
    if { [llength $srcTB] > 0 } {
      add_files -fileset sim_1 -norecurse $srcTB
    }
    if { [llength $srcXDC] > 0 } {
      add_files -fileset constrs_1 -norecurse $srcXDC
    }

    set_property top $syn_top_nm [current_fileset]
    set_property top $sim_top_nm [get_filesets sim_1]
    set_property target_simulator Questa [current_project]
    set_property compxlib.questa_compiled_library_dir $compDir [current_project]
    set_property -name {questa.simulate.runtime} -value {10ms} -objects [get_filesets sim_1]
    set_property -name {questa.simulate.log_all_signals} -value {true} -objects [get_filesets sim_1]

    report_ip_status -name ip_status
    set ipCores [get_ips]
    for {set i 0} {$i < [llength $ipCores]} {incr i} {
      set ipSingle [lindex $ipCores $i]
      set locked [get_property IS_LOCKED $ipSingle]
      set upgrade [get_property UPGRADE_VERSIONS $ipSingle]
      if { $upgrade != "" && $locked } {
        upgrade_ip $ipSingle
      }
    }
    report_ip_status -name ip_status

    #launch_runs synth_1
    #wain_on_run synth_1
    #open_run synth_1 -name synth_1
    #launch_simulation -install_path /opt/questasim/bin
  }
}

